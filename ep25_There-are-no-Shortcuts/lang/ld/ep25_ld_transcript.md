# Transcript of Pepper&Carrot Episode 25 [ld]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zha wudetha|1|False|Wud 25: Ham Ra Wohíya Woweth

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thod|1|False|RULANA

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dedidehá|13|False|- NODI -
Loyud|1|True|?
Loyud|2|True|?
Loyud|3|True|?
Loyud|4|False|?
Loyud|5|True|?
Loyud|6|True|?
Loyud|7|True|?
Loyud|8|False|?
Loyud|9|True|?
Loyud|10|True|?
Loyud|11|True|?
Loyud|12|False|?

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dohiná|1|False|Bíi thad naden ne Loyud i Ámed beth www.patreon.com/davidrevoy beha wa
Dohiná|2|False|Bíi dínonehóo Loyud i Ámed wa. Dínon woban betha. Meden wéedaná i meban ben wothem wolotheth. Áala withedim 909 hi:
Dohiná|3|False|05/2018 - www.peppercarrot.com - Alehala i Woban: David Revoy
Dohiná|4|False|Bíi nosháad dedide thera Shebedoni bethude wa. El David Revoy beth. Den Craig Maloney. Dódóon Willem Sonke, Moini, Hali, CGand i Alex Gryson.
Dohiná|5|False|Bodibod: Krita 4.0.0, Inkscape 0.92.3, Kubuntu 17.10 beha
Dohiná|6|False|Hudi: Creative Commons Attribution 4.0
Dohiná|7|False|Wodide Wowéedaná: Nicolas Artance, Imsesaok, Craig Maloney, Midgard, Valvin, xHire i Zveryok.
