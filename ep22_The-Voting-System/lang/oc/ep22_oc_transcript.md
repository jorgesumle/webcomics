# Transcript of Pepper&Carrot Episode 22 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 22 : Lo sistèma de vòte

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cònsol de Komona|1|False|Nòstre grand concors de magia pòt enfin començar !
Escritura|2|False|Concors de Magia

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cònsol de Komona|1|False|E mercés a nòstres engenhaires brilhants, vosautres tanben poiretz participar !
Cònsol de Komona|2|False|D'efièit, cars amics, admiratz aquestas petitas meravilhas tecnologicas que nòstras ostessas vos van distribuir !
Cònsol de Komona|3|False|Lo boton verd balha un punt a una candidata, lo boton roge ne tira un : a vosautres de causir !
Cònsol de Komona|4|True|« E la jurada, alara ? », m'anatz demandar.
Cònsol de Komona|5|False|Ne vos en fagatz pas, avèm pensat a tot !
Cònsol de Komona|6|False|Cada jurat recebrà una caisseta especiala, capabla de balhar o de tirar cent punts en un còp !
Pepper|7|False|E ben !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cònsol de Komona|5|False|…50 000 Ko !
Cònsol de Komona|1|False|E çò melhor, las marcas apareisseràn dirèctament al-dessús de las participantas !
Cònsol de Komona|3|False|Las tres mascas qu'auràn la marca melhora seràn seleccionadas per la finala !
Cònsol de Komona|4|True|Finala a la fin de la quala la granda venceira ganharà la polida soma de…
Escritura|2|False|1337
<hidden>|0|False|Edit this one, all others are linked
Audiéncia|6|False|Clap

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Carròt|2|False|Tap
Pepper|3|False|Es un concèpte agradiu ! È, Carròt ?
Pepper|4|True|Innovant…
Pepper|5|True|Amusant…
Pepper|6|True|Democratic…
Pepper|7|False|…lo sistèma perfièit, qué !
Pepper|8|True|I a pas pus besonh d'expèrts per jutjar la qualitat !
Pepper|9|False|Qual progrès ! Vivèm vertadièrament una epòca fantastica !
Cònsol de Komona|10|False|Cadun a sa caisseta ?
Audiéncia|11|False|Extrà !
Audiéncia|12|False|Òc !
Audiéncia|13|False|Òc !
Audiéncia|14|False|Òc !
Audiéncia|15|False|Òc !
Cònsol de Komona|16|True|Plan !
Cònsol de Komona|17|True|Que lo concors…
Cònsol de Komona|18|False|COMENCE !!
<hidden>|0|False|Edit this one, all others are linked
Audiéncia|1|False|Clap

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cònsol de Komona|1|False|E es Camomilha que dobrís lo bal !
Son|2|False|Tchiiiuu…
Camomilha|3|False|SYLVESTRIS !
Son|4|False|Bam !
Audiéncia|5|True|Tap
Audiéncia|6|True|Tap
Audiéncia|7|True|Tap
Audiéncia|8|True|Tap
Audiéncia|9|True|Tap
Audiéncia|10|True|Tap
Audiéncia|11|False|Tap

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cònsol de Komona|2|False|Camomilha obten una marca fòrça bona ! Es ara al torn de Shichimi !
Escritura|1|False|5861
Shichimi|4|False|LUX…
Son|6|False|Fii iiiii zz !!|nowhitespace
Shichimi|5|False|MAXIMA !
Audiéncia|7|False|Aaa !!
Audiéncia|8|False|Hiii !!
Audiéncia|9|False|Mos uèlhs !!!
Pepper|12|False|Carròt… Bota-li un poce verd ça que la, es nòstra amiga…
Carròt|13|False|Tap
Audiéncia|10|True|Ooooo !
Audiéncia|11|False|Boooo !
Audiéncia|14|True|Oooo !
Audiéncia|15|False|Booo !
Cònsol de Komona|17|False|A, sembla que lo public es pas estat sedusit per aquesta prestacion « brilhanta » ! Es al torn d'Espirulina !
Escritura|16|False|-42
Audiéncia|18|True|Oooo !
Audiéncia|19|True|Booo !
Audiéncia|20|True|Booo !
Audiéncia|21|False|Ooo !
<hidden>|0|False|Edit this one, all others are linked
Audiéncia|3|False|Clap

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Espirulina|1|False|RELEASUS KRAKENIS !
Son|2|False|Bbblbloo !|nowhitespace
Son|3|False|S plaa chh !|nowhitespace
Cònsol de Komona|5|False|Polit ! Potent ! Espirulina mena ara a la marca ! Coriandre, es a vos !
Escritura|4|False|6225
Espirulina e Durian|6|False|Tap
Coriandre|8|False|MORTUS REDITUS !
Son|9|False|Groo rooo !|nowhitespace
Cònsol de Komona|11|False|A, semblariá que las esqueletas sián passadas de mòda… A nòstra cara Safran !
Escritura|10|False|2023
<hidden>|0|False|Edit this one, all others are linked
Audiéncia|7|False|Clap

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Safran|1|False|Grrr ! Acceptarai pas de far mens plan qu'aquela Espirulina ! Es lo moment de balhar lo maximum !
Safran|2|False|Fai-te enlà Trufèl !
Son|3|False|Fr rrshh !|nowhitespace
Son|4|False|Fr rrshh !|nowhitespace
Son|5|False|Crchh !
Trufèl|6|False|Miau !
Safran|7|False|SPIRALIS
Safran|8|False|FLAMA aaaa aaah !|nowhitespace
Son|9|False|Fr rrooo ochh !|nowhitespace
Son|10|False|Zzziiiiip !
Son|11|False|Fr rrh !|nowhitespace
Son|12|False|Pom !
Safran|13|False|?!!
Son|14|False|Fr rrch !|nowhitespace
Son|15|False|Fr rrch !|nowhitespace
Safran|16|False|Hiiiiiiiii ! !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Safran|1|False|Ò non… Quala vergonha !
Safran|3|False|?!
Escritura|4|True|14
Escritura|5|False|849
Pepper|6|False|Tap
Lord Azeirf|7|True|Tap
Lord Azeirf|8|True|Tap
Lord Azeirf|9|False|Tap
Escritura|10|True|18
Escritura|11|False|231
Cònsol de Komona|12|True|Sioplet, sioplet ! Tenètz-vos un pauc !
Cònsol de Komona|13|False|Shichimi e Coriandre son eliminadas !
Cònsol de Komona|14|False|Camomilha, Espirulina e Safran son doncas qualificadas per la finala !
<hidden>|0|False|Edit this one, all others are linked
Audiéncia|2|False|Clap

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cònsol de Komona|1|False|Domaisèlas, sètz prèstas per aquesta darrièra espròva ?
Cònsol de Komona|2|False|A vosautras de jogar !
Escritura|3|True|1 5|nowhitespace
Escritura|4|False|703
Escritura|5|True|19
Escritura|6|False|863
Escritura|7|True|1 3|nowhitespace
Escritura|8|False|614
Son|10|False|Poum !
Pepper|11|False|Retiri tot çò qu'ai poscut dire sus aquel sistèma…
Lord Azeirf|12|True|Tap
Lord Azeirf|13|True|Tap
Lord Azeirf|14|False|Tap
Narrator|15|False|- FIN -
<hidden>|0|False|Edit this one, all others are linked
Audiéncia|9|False|Clap

### P11

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|1|False|05/2017 - www.peppercarrot.com - Dessenh & Scenari : David Revoy
Crèdits|2|False|Melhorament dels dialògues : Nicolas Artance, Valvin
Crèdits|3|False|Basat sus l'univèrs d'Hereva creat per David Revoy amb las contribucions de Craig Maloney. Correccions de Willem Sonke, Moini, Hali, CGand e Alex Gryson.
Crèdits|4|False|Logicials : Krita 3.1.3, Inkscape 0.92.1 sus Linux Mint 18.1
Crèdits|5|False|Licéncia : Creative Commons Attribution 4.0
Crèdits|6|False|Pepper&Carrot es completament liure, open source, e esponsorizat mercés al mecenat de sos lectors. Per aqueste episòdi, mercé als 864 mecènas :
Crèdits|7|False|Vos tanben, venètz mecèna de Pepper&Carrot sus www.patreon.com/davidrevoy
