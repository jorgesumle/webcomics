# Transcript of Pepper&Carrot Episode 30 [jb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|.i 30 mo'o lisri fa la'e lu be'u pamjai li'u

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|to le lisri cu mulno toi

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|5|True|.i de'i li nanca bu 2019 masti bu 9 djedi bu 3 co'a gubni .i le pixra .e le lisri cu se finti la'o fy.David Revoy.fy. .i pu cipra tcidu lu'i le lisri poi pu nu'o bredi fa la'o gy.CalimeroTeknik.gy. e la'o gy.Craig Maloney.gy. e la'o gy.Jihoon Kim.gy. e la'o gy.Parnikkapore.gy. e la'o gy.Martin Disch.gy. e la'o gy.Midgard.gy. e la'o gy.Nicolas Artance.gy. e la'o gy.Valvin.gy. .i le lojbo xe fanva zo'u fanva fa la gleki .i skicu la .erevas. noi munje zi'e noi finti fa la'o.fy.David Revoy.fy. .i ralju sidju fa la'o gy.Craig Maloney.gy. .i finti lei lisri be la .erevas. fa la'o gy.Craig Maloney.gy. e la'o gy.Nartance.gy. e la'o gy.Scribblemaniac.gy. e la'o gy.Valvin.gy. .i cikre le xe fanva fa la'o gy.Willem Sonke.gy. e la'o gy.Moini.gy. e la'o gy.Hali.gy. e la'o gy.CGand.gy. e la'o gy.Alex Gryson.gy. .i pilno le proga poi du la'o py.Krita/4.2~git branch.py. jo'u la'o py. Inkscape 0.92.3.py. jo'u la'o py.Kubuntu 18.04.2.py. i finkrali javni fa la'o jy. Creative Commons Attribution 4.0 .jy .i zoi .urli. www.peppercarrot.com .urli. judri
la .piper.|3|True|la'e la .piper. jo'u la .karot. cu fingubni gi'e se sarji le tcidu
la .piper.|4|False|.i sarji le nu finti le dei lisri kei fa le 973 da poi liste fa di'e
la .piper.|7|True|xu do djuno le du'u
la .piper.|6|True|.i do ji'a ka'e sidju .i je ba'e le cmene be do ba se ciska fi le tai papri
la .piper.|8|False|.i ko tcidu fi la'o .url. www.peppercarrot.com .url. ja'e le nu do djuno fi le tcila
la .piper.|2|True|.i mi'a pilno la'o gy.Patreon.gy. e la'o gy.Tipeee.gy. e la'o gy.PayPal.gy. e la'o gy.Liberapay.gy. e la'o gy. e lo drata
Credits|1|False|ki'e do
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
