# Transcript of Pepper&Carrot Episode 02 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 2: Regnbueeliksirerne

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|5|True|Glug
Lyd|6|True|Glug
Lyd|7|False|Glug
Skrift|1|True|PAS PÅ
Skrift|3|False|PRIVAT
Skrift|2|True|HEKSEN
Skrift|4|False|33

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|7|False|dunk|nowhitespace
Skrift|1|False|FIRE D
Skrift|2|False|DEEP OCEAN
Skrift|3|False|VIOLE(N)T
Skrift|4|False|ULTRA BLUE
Skrift|5|False|PIN
Skrift|6|False|MSON
Skrift|8|False|NATURE
Skrift|9|False|YELLOW
Skrift|10|False|ORANGE TOP
Skrift|11|False|FIRE DANCE
Skrift|12|False|DEEP OCEAN
Skrift|13|False|VIOLE(N)T
Skrift|14|False|ULTRA BLUE
Skrift|15|False|META PINK
Skrift|16|False|MAGENTA X
Skrift|17|False|CRIMSON

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|True|Glug
Lyd|2|True|Glug
Lyd|3|False|Glug

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|True|Glup
Lyd|2|False|Glup
Lyd|3|True|Glup
Lyd|4|False|Glup
Lyd|5|True|Glup
Lyd|6|False|Glup
Lyd|20|False|pff|nowhitespace
Lyd|19|True|m|nowhitespace
Lyd|18|True|M|nowhitespace
Lyd|7|True|B|nowhitespace
Lyd|8|True|v|nowhitespace
Lyd|9|True|a|nowhitespace
Lyd|10|False|dr!|nowhitespace
Lyd|11|True|S|nowhitespace
Lyd|12|True|S|nowhitespace
Lyd|13|True|S|nowhitespace
Lyd|14|True|P|nowhitespace
Lyd|15|True|l|nowhitespace
Lyd|16|True|a|nowhitespace
Lyd|17|False|t|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|True|B
Lyd|2|True|lup|nowhitespace
Lyd|3|True|B
Lyd|4|False|lup|nowhitespace
Lyd|7|True|up|nowhitespace
Lyd|6|True|pl|nowhitespace
Lyd|5|True|s
Lyd|10|True|up|nowhitespace
Lyd|9|True|pl|nowhitespace
Lyd|8|True|s
Lyd|13|False|up|nowhitespace
Lyd|12|True|pl|nowhitespace
Lyd|11|True|s

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|True|Denne tegneserie er open-source, og denne episode er finansieret af 21 tilhængere:
Credits|2|False|www.patreon.com/davidrevoy
Credits|3|False|Mange tak til:
Credits|4|False|Lavet med Krita på GNU/Linux
