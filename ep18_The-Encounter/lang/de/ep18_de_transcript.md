# Transcript of Pepper&Carrot Episode 18 [de]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 18: Die Begegnung

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|"Hippiāh" subtle on the box
Pepper|1|False|OK, du kannst jetzt schauen!
Pepper|2|True|TA~DAA~~A !|nowhitespace
Pepper|3|True|Eine Hippiāh-Hexenrobe!
Pepper|4|False|Es war die letzte, ich hab sie genommen, bis der Laden meine Roben wieder reinbekommt.
Pepper|7|False|Und? Erinnert dich das an etwas?...
Schrift|6|False|HIPPIĀH

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Wood panel
<hidden>|0|False|Door
Schrift|1|False|Hexen-schule von Hippiāh
Schrift|3|False|KÜCHE
Carrot|2|False|Knurr
Basilikum|4|False|Oregano, gut.
Basilikum|5|False|Kardamom, gut.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Basilikum|1|False|Zimt, sehr gut.
Basilikum|2|False|Kamille, gut.
Basilikum|3|False|Pepper ?!
Pepper|4|False|Aber ich...
Basilikum|5|True|Wieder versagt!!!
Basilikum|6|False|Muss ich Dich dran erinnern, dass Hippiāh-Magie nicht zum Unkraut-vernichten da ist!...
Oregano|7|False|HA HAHA HA !
Kardamom|8|False|HA HA !
Zimt|9|False|HA HA !
Kamille|10|False|HA HAHA HA !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Oregano|1|False|HAHA HA !
Kardamom|2|False|HuHu Hu !
Zimt|9|False|Pfff !
Kamille|10|False|HAHA HA HA !
Pepper|3|True|Hört auf
Pepper|4|True|!!!
Pepper|5|True|Es ist nicht meine Schuld!!!
Pepper|6|True|Kommt schon...
Pepper|7|True|STOP
Pepper|8|False|!!!
Kardamom|12|False|HAHA HA HA !
Oregano|11|False|HuHu Hu !
Kamille|14|False|HAHA HA HA !
Zimt|15|False|HAHA HA !!
Pepper|13|False|Grr... Hört auf... Ich sagte...
Pepper|16|True|Ich sagte...
Pepper|17|False|HÖRT AUF !!!
Geräusch|18|False|B A AAM ! ! !|nowhitespace
Geräusch|19|False|K L IR RR ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|on the door
Cayenne|1|True|Ähhem...
Cayenne|2|False|Wenn Sie wollen, kümmern wir uns um sie...
Cayenne|3|False|Wie der Zufall so will, suchen wir eine Schülerin für Chaosāh...
Thymian|4|False|... und sie scheint die perfekte Wahl zu sein.
Basilikum|5|True|Cayenne?!
Basilikum|6|True|Thymian?! Kümmel?!
Basilikum|7|False|Aber ich dachte Ihr seid alle... ... das ist nicht möglich!
Schrift|8|False|KÜCHE

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geräusch|1|False|wuschhh !|nowhitespace
Geräusch|2|False|Bamm !|nowhitespace
Pepper|3|False|Hii hii hi!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|"Hippiāh" subtle on the box
<hidden>|0|False|Don't translate the names of the scenarios
Pepper|1|True|Ich habe das Gefühl, wir werden ein gutes Team!
Pepper|2|False|... und ich weiß auch schon, wie ich dich nennen werde!
Pepper|3|False|Also? An was erinnert dich das?!
Carrot|4|False|Knurr?
Schrift|5|False|HIPPIĀH
Erzähler|6|False|- ENDE -
Impressum|7|False|Die Szenarien sind von zwei vorgeschlagenen Szenenien von Craig Maloney inspiriert: "You found me, I Choose You" und "Visit from Hippiah".
Impressum|8|False|Lizenz : Creative Commons Namensnennung 4.0, Software: Krita 3.0, Inkscape 0.91 on Manjaro XFCE
Impressum|9|False|Basierend auf dem Hereva Universum von David Revoy mit Unterstützung von Craig Maloney. Korrekturen von Willem Sonke, Moini, Hali, CGand und Alex Gryson.
Impressum|10|False|08/2016 - www.peppercarrot.com - Grafik & Handlung : David Revoy - Deutsche Übersetzung : Philipp Hemmer

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Impressum|1|False|Pepper&Carrot ist komplett frei, Open Source und wird durch die Leser unterstützt und finanziert. Für diese Episode geht der Dank an die 720 Förderer:
Impressum|2|False|Auch Du kannst Pepper&Carrot unterstützen: www.patreon.com/davidrevoy
