# Transcript of Pepper&Carrot Episode 06 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 6: Trylledrikk-konkurransen

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Å søren, eg sovna visst med vindauget ope att ...
Pepar|2|False|... men for ein vind ...
Pepar|3|False|... og kvifor kan eg sjå Komona gjennom vindauget?
Pepar|4|False|KOMONA!
Pepar|5|False|Konkurransen!
Pepar|6|False|Eg må ha ... ... ha sovna ved eit uhell!
Pepar|7|True|... men?
Pepar|8|False|Kvar er eg?!
Fugl|10|False|kk?|nowhitespace
Fugl|9|True|Kva|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|!!!
Pepar|2|False|Gulrot! Så søtt og omtenksamt av deg å fly meg til konkurransen!
Pepar|3|False|Fan-tas-tisk!
Pepar|4|False|Du hugsa til og med på å ta med ein trylledrikk, kleda mine og hatten min.
Pepar|5|False|La meg no sjå kva brygg du tok med ...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|KVA?!
Ordføraren i Komona|3|False|Som ordførar av Komona har eg no æra av å opna trylledrikk-konkurransen!
Ordføraren i Komona|4|False|Det er ei stor glede for byen vår å ynskja ikkje mindre enn fire hekser velkomne til denne fyrste tevlinga.
Skrift|2|False|Komona trylledrikk-konkurranse
Ordføraren i Komona|5|True|La oss gje ein
Ordføraren i Komona|6|True|stor
Ordføraren i Komona|7|False|applaus til ...

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Publikum|29|False|Klapp
Ordføraren i Komona|1|True|Heilt frå den store Teknologunionen: Det er ei stor ære å presentera fortryllande og gløgge
Ordføraren i Komona|3|True|Me må heller ikkje gløyma vår lokale heks, Komonas eiga
Ordføraren i Komona|5|True|Frå landet med månenedgangane har me med oss vår tredje deltakar,
Ordføraren i Komona|7|True|Og heilt til slutt, frå Ekornstubben, kjem den siste deltakaren vår,
Ordføraren i Komona|2|False|Koriander!
Ordføraren i Komona|4|False|Safran!
Ordføraren i Komona|6|False|Shichimi!
Ordføraren i Komona|8|False|Pepar!
Ordføraren i Komona|9|True|La konkurransen byrja!
Ordføraren i Komona|10|False|Vinnaren vert kåra med applausmålar.
Ordføraren i Komona|11|False|Fyrste oppvisinga er det Koriander som står for.
Koriander|13|False|... ver ikkje lenger redde for dauden, takka vere ...
Publikum|16|True|Klapp
Publikum|17|True|Klapp
Publikum|18|True|Klapp
Publikum|19|True|Klapp
Publikum|20|True|Klapp
Publikum|21|True|Klapp
Publikum|22|True|Klapp
Publikum|23|True|Klapp
Publikum|24|True|Klapp
Publikum|25|True|Klapp
Publikum|26|True|Klapp
Publikum|27|True|Klapp
Publikum|28|False|Klapp
Koriander|12|False|Mine damer og herrar ...
Koriander|14|True|ZOMBIE-BRYGGET
Koriander|15|False|mitt!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ordføraren i Komona|1|False|FANTASTISK!
Publikum|3|True|Klapp
Publikum|4|True|Klapp
Publikum|5|True|Klapp
Publikum|6|True|klapp
Publikum|7|True|Klapp
Publikum|8|True|Klapp
Publikum|9|True|Klapp
Publikum|10|True|Klapp
Publikum|11|True|Klapp
Publikum|12|True|Klapp
Publikum|13|True|Klapp
Publikum|14|True|Klapp
Publikum|15|True|Klapp
Publikum|16|False|Klapp
Safran|18|False|for her har de brygget mitt !
Safran|17|True|... men spar på applausen, godtfolk,
Safran|23|False|mitt!
Safran|21|False|Alt dette er mogleg ved å nytta berre ein einaste liten drope av ...
Publikum|24|True|Klapp
Publikum|25|True|Klapp
Publikum|26|True|Klapp
Publikum|27|True|Klapp
Publikum|28|True|Klapp
Publikum|29|True|Klapp
Publikum|30|True|Klapp
Publikum|31|True|Klapp
Publikum|32|True|Klapp
Publikum|33|True|Klapp
Publikum|34|True|Klapp
Publikum|35|True|Klapp
Publikum|36|True|Klapp
Publikum|37|False|Klapp
Ordføraren i Komona|39|False|Denne drikken kan gjera alle i Komona styrtrike!
Ordføraren i Komona|38|False|Fantastisk! Utruleg!
Publikum|40|True|Klapp
Publikum|41|True|Klapp
Publikum|43|True|Klapp
Publikum|42|True|Klapp
Publikum|44|True|Klapp
Publikum|45|True|Klapp
Publikum|46|True|Klapp
Publikum|47|True|Klapp
Publikum|48|True|Klapp
Publikum|49|True|Klapp
Publikum|50|True|Klapp
Publikum|51|True|Klapp
Publikum|52|True|klapp
Publikum|53|True|Klapp
Publikum|54|True|Klapp
Publikum|55|True|Klapp
Publikum|56|False|Klapp
Ordføraren i Komona|2|False|Koriander trassar sjølvaste dauden med denne mi-ra-ku-løse trylledrikken!
Safran|19|False|Det de alle har venta på – ein trylledrikk som vil gje naboane hakeslepp ...
Ordføraren i Komona|57|False|Applausen dykkar er ikkje til å ta feil av. Koriander er alt ute av konkurransen.
Safran|20|False|... og gjera dei grøne av misunning!
Safran|22|True|... SNOBBEBRYGGET

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Ordføraren i Komona|1|False|Det vert vanskeleg for Shichimi å overgå den framvisinga der!
Shichimi|2|False|Eg ... eg visste ikkje at me måtte visa det fram ...
Ordføraren i Komona|3|False|Kom igjen, Shichimi – alle ventar på deg ...
Shichimi|4|True|NEI !|nowhitespace
Shichimi|5|True|Eg kan ikkje. Det er for farleg!
Shichimi|6|False|ORSAK!
Ordføraren i Komona|7|False|Mine damer og herrar, det ser ut til at Shichimi let sjansen gå frå seg ...
Safran|8|False|Gje meg den!
Safran|9|False|... slutt å spela sjenert – du øydelegg showet.
Safran|10|False|Dessutan veit alle at eg har vunne, same kva denne trylledrikken din kan gjera ...
Shichimi|11|False|!!!
Lyd|12|False|B Z Z Z I IO O|nowhitespace
Shichimi|13|True|PASS PÅ!!!
Shichimi|14|True|Trylledrikken skapar
Shichimi|15|False|KJEMPEMONSTER!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fugl|1|False|KA-KAH-K aa aa aa aaa a a a|nowhitespace
Lyd|2|False|BAM!
Pepar|3|True|He ... kult!
Pepar|5|False|Trylledrikken min vil i det minste gje oss ein god latter, for ...
Pepar|4|False|Er det min tur no?
Ordføraren i Komona|6|False|Spring, din tosk!
Ordføraren i Komona|7|False|Konkurransen er over! Berg deg sjølv!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|2|False|Som vanleg dreg alle når det er vår tur.
Pepar|1|True|Typisk!
Pepar|4|False|... nemleg å få alt normalt att og få oss heim!
Pepar|9|False|Vil du smaka eit siste brygg?
Pepar|10|False|Ikkje det, nei?
Pepar|5|False|HEI!
Lyd|8|False|K N U S !|nowhitespace
Pepar|3|False|Iallfall veit eg kva me kan gjera med «brygget» ditt, Gulrot ...
Pepar|6|True|Du din forvaksne
Pepar|7|False|snobbe-zombie-pippip!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Ja, sjå nøye på merke-lappen ...
Pepar|2|False|Eg kjem ikkje til å nøla med å tømma alt saman over deg om ikkje du pellar deg ut av Komona no med ein gong!
Ordføraren i Komona|3|True|Fordi ho redda byen vår frå undergang ...
Ordføraren i Komona|4|False|... g å r fyrstepremien til Pepar for trylledrikken som ...?!
Pepar|6|False|... eh ... ... som faktisk ikkje er ein trylledrikk. Det er ein urinprøve frå sist Gulrot var hos veterinæren!
Pepar|5|True|Ha-ha! Jau ...
Pepar|7|False|Så ... ingen demonstrasjon?
Forteljar|9|False|Episode 6: Trylledrikk-konkurransen
Forteljar|10|False|Slutt
Skrift|8|False|50 000 Ko
Bidragsytarar|11|False|Mars 2015 – Teikna og fortald av David Revoy – Omsett av Arild Torvund Olsen og Karl Ove Hufthammer

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane. Takk til dei 245 som støtta denne episoden:
Bidragsytarar|4|False|https://www.patreon.com/davidrevoy
Bidragsytarar|3|False|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot:
Bidragsytarar|7|False|Verktøy: Episoden var laga med 100 % fri programvare: Krita på Linux Mint.
Bidragsytarar|6|False|Opne kjelder: Alle kjeldefilene og skrifttypane er tilgjengelege på den offisielle nettsida.
Bidragsytarar|5|False|Lisens: Creative Commons Attribution. Du kan byggja vidare på episoden, dela han, selja han osb.
Bidragsytarar|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
