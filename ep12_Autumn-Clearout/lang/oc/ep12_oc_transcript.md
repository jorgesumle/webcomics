# Transcript of Pepper&Carrot Episode 12 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 12 : Recapte d'auton

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|9|True|Clinc
Son|10|True|Clanc
Son|11|False|Clonc
Cayenne|1|False|Pocions cacalàs
Cayenne|2|False|Pocions pel subrebèl
Cayenne|3|False|Pocion bòlas pudentas
Cayenne|4|False|Pocion la vida en ròse
Cayenne|5|False|Pocion fumigèn...
Cayenne|6|False|...e ne passi !
Pepper|7|True|Òc, o sabi :
Pepper|8|False|« Una-vertadièra-masca-de-Caosah-deu-pas-far-aquela-mena-de-pocions. »
Cayenne|13|True|Devi anar al mercat de Komona lèu fait.
Cayenne|14|False|Fai-me desaparéisser tot aquò abans que torne ; seriá domatge que de petits galejaires jòguen amb aqueles daquòs.
Cayenne|12|False|Òc. E es plan melhor atal.

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|2|False|Zioom !|nowhitespace
Pepper|3|False|Cotral !
Pepper|4|True|Tot enterrar ?!
Pepper|5|False|Mas prendrà d'oras e d'oras, e aquò sens parlar de las botiòlas a las mans !
Son|6|False|Pom
Pepper|1|False|Far desaparéisser ? Es aisit ! Zo !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|« E sustot, utilizes pas la magia. Una vertadièra masca de Caosah utiliza pas la magia per de trabalhs de la vida vidanta. »
Pepper|2|False|Grrr !
Pepper|3|True|CARRÒT !
Pepper|4|False|Libre seten de Caosah : « los Camps Gravitacionals »... ...còp sec !
Pepper|5|False|Li vau mostrar çò qu'es una vertadièra masca de Caosah a aquela vièlha cabra !
Son|6|True|Tam
Son|7|False|Tam

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|2|False|VRO O oo oo oo oo oo|nowhitespace
Cayenne|3|False|...Un trauc negre de Caosah?!...
Pepper|4|False|?!
Cayenne|5|False|...De vertat, es aquò qu'ausisses quand disi : « utilizar pas la magia » ?
Pepper|6|False|... Bèèè... Sètz pas supausada èstre al mercat ?
Cayenne|7|True|Que non pas, solide !
Cayenne|8|False|E aguèri plan rason ! Es pas possible de te daissar sens susvelhança !
Pepper|1|False|GURGES ATER !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|òuuu òu uòo owoo|nowhitespace
Son|7|False|Òuuuòu uòuu uòuoo|nowhitespace
Son|6|False|Bam
Carròt|10|False|?!
Cayenne|2|False|...E gaita-me aquò.
Cayenne|3|True|Pas pro de massa !
Cayenne|4|True|Camp gravitacional diferencial feble !
Cayenne|5|False|A mai la darrièra orbita circulària establa es tròp pichona !
Cayenne|9|False|Alara qu'un orizont dels eveniments un bricon mai grand auriá convengut !
Cayenne|8|False|E vaquí : Quasi tot gravitarà dins l'orbita establa o als punts de Lagrange e contunharà de flotar a l'entorn.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|! ! !|nowhitespace
Cayenne|2|False|! ! !|nowhitespace
Cayenne|3|False|CURSUS ANNULATIONUS MAXIMUS!
Son|4|False|Chc lac!|nowhitespace
Narrator|5|False|- FIN -
Crèdits|6|False|10/2015 - Dessenh & Scenari : David Revoy

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|3|False|https://www.patreon.com/davidrevoy
Crèdits|4|False|Licéncia : Creative Commons Attribution 4.0 Sorsas : disponiblas sus www.peppercarrot.com Logicials : aqueste episòdi foguèt dessenhat 100% amb de logicials liures Krita 2.9.8, Inkscape 0.91, Blender 2.71, GIMP 2.8.14, G'MIC 1.6.7 sus Linux Mint 17
Crèdits|1|False|Pepper&Carrot es completament liure, open source, e esponsorizat mercés al mecenat de sos lectors. Per aqueste episòdi, mercé als 575 mecènas :
Crèdits|2|True|Vos tanben, venètz mecèna de Pepper&Carrot per l'episòdi venent sus
