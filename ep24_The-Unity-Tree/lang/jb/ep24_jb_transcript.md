# Transcript of Pepper&Carrot Episode 24 [jb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|.i 24 mo'o lisri le tricu be la nu kansa
<unknown>|2|False|la .piper. joi la .karot.

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|False|PAF !|nowhitespace
la .kaien.|1|True|.i do djica tu'a lo tricu be la nu kansa
la .kaien.|4|False|.i mi'o noi me le te makfa pe la kalsa ba'e na zukte lo simsa
la .kaien.|6|False|.i .e'u do jundi tadni le valsi be la nu daspo
la .piper.|7|True|la xu nu daspo
la .piper.|8|True|ca le nau citsi
la .piper.|9|False|.i pei se ba'i ku tadni lo valsi be la nu zbasu
la .kaien.|10|False|.i pu'o penmi lu'a le kamni be do'e le ci lunra .i se ni'i bo pu'o cipra fi do .i ko tadni gi'e ba'e tadni .i ko co'u pensi lo jai se bebna sidbo
la .kaien.|2|False|bu'u ti .oi
la .kaien.|3|True|.e'anaisai

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|.b a m.|nowhitespace
la .piper.|5|True|.i dunra .i no da tricu la nu kansa
la .piper.|7|False|.i pei le nu zbasu le tricu cu makfa fi ba'e mi
la .piper.|8|True|.i .u'a zabna citsi le nu mi pa roi pilno le makfa pe la kalsa ba'e jo'u la rarna
la .piper.|9|True|.i .iu ve'u crino tricu gi'e se gusni fi le tarci .e le ciste be fi le solri
la .piper.|10|False|.i .u'e da'i prane
la .piper.|11|True|.i da'i se tcila le
la .piper.|12|False|ba'e cnino
la .piper.|13|True|.i ku'i no da curmi le nu mi zukte le simsa bu'u ti
la .piper.|14|False|.i da'i lei kurji zi sanji le nu mi na tadni le mi valsi be la nu daspo
la .karot.|3|False|tap
Sound|2|False|Ffff
la .piper.|6|False|.i .ei no roi
la .piper.|4|False|le'o .oi

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|3|False|.cryr.
Sound|17|False|.zving.
Writing|22|False|la .komonan.
la .piper.|1|True|.i ku'i
la .piper.|2|False|ba'a nai bu'o
la .piper.|4|False|.i .ie le ckupau be la'e la valsi be la nu daspo .i la .timian. cu stidi le nu pilno le valsi bu'u le sivni se cimde .i ku'i ty. xusra le du'u ckape
la .piper.|16|False|.i .ai gunka
la .kumin.|19|False|pe'i la .piper. pu zi zbasu le sivni se cimde
Sound|18|False|.cuuuuump.|nowhitespace
la .timian.|20|True|zabna
Writing|8|False|.i lu .e'i le se cupra be bu'u le sivni ba'e na co'a zvati le gubni munje .i nu bu ckape li'u
Writing|6|False|lu ca le nu do zbasu le sivni se cimde vau ko ba'e na gasnu le mu'e xaksu le livla be do li'u
Writing|11|True|lu ca le nu do zvati le sivni zo'u no drata ka'e sidju do
la .piper.|7|False|.i li'a
la .piper.|10|False|.i .ua
la .piper.|9|False|ckape .a'u nai li'a
la .piper.|5|False|.y.
Writing|12|False|.i je no da ka'e viska do li'u
la .timian.|21|False|.i .a'o ra morji fi le ckape voi mi pu ciska le jufra be ke'a le ckupau be la'e la valsi be la nu daspo
la .piper.|15|True|.i prane
la .piper.|13|True|.i ba'e no xu da
la .piper.|14|True|.i je'u pei

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|1|True|.u'esai
la .piper.|2|True|.i le sivni se cimde cu tanxe ti po'o voi mi nitcu .i mulno sepli le munje
la .piper.|3|False|.i prane stuzi le cipra nu pilno le valsi
la .piper.|4|False|.i .ei mi jundi le se finti be mi
Sound|5|False|.cuuing.
Sound|6|False|.pluuum.|nowhitespace
Sound|8|False|.duuuf.|nowhitespace
Sound|10|False|.ctring.|nowhitespace
la .piper.|7|False|.y. na snada
la .piper.|9|False|.iunai
la .piper.|11|False|.i .ui .ie zabna .i ku'i cmalu
la .piper.|13|False|DOI .KAROT. KO NA PENCU
la .karot.|12|False|y

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|.pluuum.|nowhitespace
la .piper.|3|True|.i pe'i da'i na betri fa le nu le tricu gau muvdu le gubni munje
la .piper.|2|True|.i .uo dekpu li rau .i simlu le ka prane
la .piper.|5|True|.i citsi le nu
la .piper.|6|True|le se cimde co'a pagbu le ralju munje .i .au lei kurji cu zgana le nu la kalsa ka'e je'a zabna
Sound|8|False|.duuung.|nowhitespace
la .timian.|9|True|.i .uo la .piper. di'a zvati
la .timian.|10|False|.i .i'e doi la .kaien. .i le do tadji co'a rinka le xamgu
la .kaien.|11|False|ki'e jatna
la .piper.|7|False|.i .au catlu le flira be le kurji
Writing|12|False|la .komonan.
la .piper.|4|False|.i ti tricu sa'u la nu kansa gi'e na srana le valsi be la nu daspo

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|5|True|brururur
Sound|6|True|brurur
Sound|7|True|brururur
Sound|8|True|brururur
Sound|9|True|brururur
Sound|10|False|brurur
la .karot.|11|False|y
la .karot.|13|False|y .y
Sound|14|True|.brurur.
Sound|15|False|.brururur.
la .karot.|16|False|!!
la .piper.|1|True|doi .karot.
la .piper.|2|True|mi ba ze'i tavla la .timian. .e la .kaien. .e la .kumin.
la .piper.|3|False|.i ko na pencu vi'o pei
la .piper.|4|False|.i mi ba zi di'a zvati
la .piper.|17|True|.i ko bredi le ka se spaji gi'e manci .i do no roi zgana lo simsa
la .piper.|18|False|.i .e'o kansa mi tu'a le galtu kumfa .y.
Sound|12|False|.cruuuf.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|6|False|юI FANMO
Sound|1|False|.cruuuuuuuf.
Sound|4|False|.darx.|nowhitespace
Sound|3|False|.janl.|nowhitespace
la .piper.|2|False|mo
la .piper.|5|False|юi za'a mi za'o co'a certu le ka pilno le valsi be la nu daspo

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|2|False|i de'i li nanca bu 2017 masti bu 12 - www.peppercarrot.com - Art & Scenario: David Revoy
Credits|3|False|Dialog Improvements: Craig Maloney, CalimeroTeknik, Jookia.
Credits|5|False|Based on the universe of Hereva created by David Revoy with contributions by Craig Maloney. Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.
Credits|6|False|Software: Krita 3.2.3, Inkscape 0.92.2 on Ubuntu 17.10
Credits|7|False|Licence: Creative Commons Attribution 4.0
Credits|9|False|You too can become a patron of Pepper&Carrot at www.patreon.com/davidrevoy
Credits|8|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thanks go to 810 Patrons:
Credits|4|False|Proofreading/Beta feedback: Alex Gryson, Nicolas Artance, Ozdamark, Zveryok, Valvin and xHire.
Credits|1|False|au do zabna salci
