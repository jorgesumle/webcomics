# Transcript of Pepper&Carrot Episode 15 [ld]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zha wudetha|1|False|Wud 15: Wowed Wobab

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dedidehá|1|False|- NODI -
Dohiná|2|False|03/2016 - Alehala i Woban: David Revoy

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dohiná|1|False|Bíi dínonehóo Loyud i Ámed wa. Dínon woban betha. Meden wéedaná i meban ben wothem wolotheth. Áala withedim 686 hi:
Dohiná|2|True|Bíi thad naden ne Loyud i Amed beth wudewan aril nuha:
Dohiná|3|False|https://www.patreon.com/davidrevoy
Dohiná|4|False|Hudi: Creative Commons Attribution 4.0 Bíi ham woban www.peppercarrot.com beha Bodibod: Bíi wudeth hi thodeshub wodódin wobodibodenan wa Krita 2.9.11, Inkscape 0.91, Linux Mint 17 beha
