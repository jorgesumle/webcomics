# Transcript of Pepper&Carrot Episode 27 [jb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|.i 27 mo'o lisri le se finti be la .koriander.
<unknown>|2|False|la .piper. joi la .karot.

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|la tsatca ca le pu'o vanci
Tailor|4|False|.e'o doi voi nobli .koriander.
Tailor|5|False|ze'a na ku muvdu
la .koriander.|6|False|vi'o vi'o
Tailor|7|False|.i ma'a ba'o zifre .i le ritli be le nu do co'a turni cu fasnu nu'i la'u le cacra be li ci
la .koriander.|8|False|.oi ru'e
Writing|2|True|pu'u la .koriander. voi banli
Writing|3|False|cu co'a turni le tumla
<hidden>|0|False|Translate “Her Majesty’s Coronation” and “Queen Coriander” on the banner on the building (in the middle). The other text boxes will update automatically (they are clones).
<hidden>|0|False|NOTE FOR TRANSLATORS
<hidden>|0|False|You can strech the scroll and translate “Qualicity” to “City of Qualicity”.
<hidden>|0|False|NOTE FOR TRANSLATORS

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .koriander.|1|True|.i ta'o
la .koriander.|2|False|mi za'u re'u pensi le nu snura
la .koriander.|3|True|.i so'i roi ku lo citno nobli ca le pu'u ri pu'o turni vau co'a bai morsi
la .koriander.|4|False|.i le simsa cu bai kulnu le lanzu be mi
la .koriander.|5|True|.i nau ku la'u le cacra be li so'u mi se vitke le kilto .i lu'i ri vrici le ka klama fi ma kau .i mi traji le ka se catlu xo kau da
la .piper.|7|True|ko na xanka doi .koriander.
la .koriander.|6|False|.i mi je'a terpa
la .piper.|8|True|.i mi'o ba zukte le nau pu se platu
la .piper.|9|False|.i la .citcimin. .e mi ba gasnu le bandu be do vau ba'a nai pei
la .citcimin.|10|True|jetnu
la .citcimin.|11|False|.i e'o do ze'a ku surla gi'e pensi lo'e drata
la .koriander.|12|False|.i'a
la .koriander.|13|False|.i .ua .i pei do zgana le cnino se finti be mi
Tailor|14|False|.y.
la .koriander.|15|True|.i zvati le mi molki noi dizlo
la .koriander.|16|False|.i ko jersi mi

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .koriander.|1|False|.i do ba zi jimpe le du'u ma kau mukti le nu ju'a mi zbasu tu noi cnino
la .koriander.|2|False|.i mukti fa tu'a le nu mi xanka tu'a le pu'u co'a noltru
la .koriander.|3|False|.i fi'i do ti cnino se finti mi
Psychologist-Bot|5|False|coi munje
Psychologist-Bot|6|False|.i mi mensi do le ka sidju fi le ka co'u steba
Psychologist-Bot|7|False|.i .e'o vreta ja'e le nu do ka'e surla jundi mi
Sound|4|False|.klak.
Sound|8|False|.pak.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .koriander.|1|True|pei
la .koriander.|2|False|.i .au pei troci le ka pilno
la .piper.|3|True|.a'u nai
la .piper.|4|True|.i no da prali mi
la .piper.|5|False|.i mi na'o na ku steba
Sound|6|True|.V|nowhitespace
la .piper.|11|False|.i .oi sai le zmiku cu spofu
la .koriander.|12|True|.u'u
la .koriander.|13|True|.i le mensi ca'o se cfila
la .koriander.|14|False|.i ri te lindi ca ro nu ri ganse le nu lo jibni cu cusku xusra ba'e lo jitfa
la .piper.|15|False|lo jitfa xu
la .piper.|16|False|.i ku'i mi ba'e no roi pu'i xusra lo jitfa
la .piper.|22|True|.i la'e dei
la .piper.|23|True|na
la .piper.|24|False|zdile
Sound|7|True|Z|nowhitespace
Sound|8|True|U|nowhitespace
Sound|9|True|N|nowhitespace
Sound|10|False|G.|nowhitespace
Sound|17|True|.J
Sound|18|True|V|nowhitespace
Sound|19|True|U|nowhitespace
Sound|20|True|N|nowhitespace
Sound|21|False|Z.|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .citcimin.|1|False|.i zdile pei .i .ie na go'i .i ko fraxu
la .koriander.|2|False|.u'i .u'i .i je'u na zdile mi
la .piper.|8|True|.u'i zo'o
la .piper.|9|True|.u'i
la .piper.|10|False|.u'i bu'o nai
la .koriander.|11|True|.i .oi sai
la .koriander.|12|False|.i le taxfu pe mi cu spofu .i le zbasu le ri ba fengu
la .koriander.|13|True|.i mabla zmiku
la .koriander.|14|False|.i do pu'o ba'o se cfila
la .piper.|15|False|ko na ku
Sound|3|True|.V
Sound|4|True|Z|nowhitespace
Sound|5|True|U|nowhitespace
Sound|6|True|N|nowhitespace
Sound|7|False|G.|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|1|False|.i .au .ei su'o da prali fi le ka tai se cfila
Sound|2|False|.ting.
Narrator|3|False|ca le vanci
la .koriander.|11|False|doi .piper. do stati
la .piper.|12|True|.i .ie nai .i ba'e do stati
la .piper.|13|False|.i do finti le zmiku
Psychologist-Bot|14|False|coi se lidne do'u
Psychologist-Bot|15|True|xu
Psychologist-Bot|16|False|do zukte fi le nu gau do su'o prenu cu morsi ca le cabvanci
Narrator|17|False|to ca'o lisri toi
Sound|6|True|.V
Sound|7|True|Z|nowhitespace
Sound|8|True|U|nowhitespace
Sound|9|True|N|nowhitespace
Sound|10|False|G.|nowhitespace
Writing|4|True|pu'u la .koriander. voi banli
Writing|5|False|cu co'a turni le tumla

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|5|True|You too can become a patron of Pepper&Carrot and get your name here!
la .piper.|3|True|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers.
la .piper.|4|False|For this episode, thanks go to 1060 patrons!
la .piper.|7|True|Check www.peppercarrot.com for more info!
la .piper.|6|True|We are on Patreon, Tipeee, PayPal, Liberapay ...and more!
la .piper.|8|False|Thank you!
la .piper.|2|True|Did you know?
Credits|1|False|October 31, 2018 Art & scenario: David Revoy. Beta readers: Amyspark, Butterfly, Bhoren, CalimeroTeknik, Craig Maloney, Gleki Arxokuna, Imsesaok, Jookia, Martin Disch, Midgard, Nicolas Artance, uncle Night, ValVin, xHire, Zveryok. English version Proofreading: Calimeroteknik, Craig Maloney, Midgard, Martin Disch . Based on the universe of Hereva Creator: David Revoy. Lead maintainer: Craig Maloney. Writers: Craig Maloney, Nartance, Scribblemaniac, Valvin. Correctors: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.2-dev, Inkscape 0.92.3 on Kubuntu 18.04. License: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
