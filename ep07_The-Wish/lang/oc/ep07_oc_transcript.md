# Transcript of Pepper&Carrot Episode 07 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 7 : Lo vòt

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|CARRÒT !
Pepper|2|False|CARRÒT !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Carròt|1|True|Zzzzz
Carròt|2|False|Zzzzz
Carròt|5|True|Zzzzz
Carròt|6|False|Zzzzz
Pepper|3|False|A ! Siás aquí !
Pepper|4|False|B Z Z Z
Son|7|False|PO F !|nowhitespace
Son|8|False|Anem, es acabat de far nonet, tornam a l'ostal !|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fadas|1|True|Planvenguts... dins la balma magica de las fadas, aventurièrs...
Pepper|3|False|...qual que... ...siá
Fadas|2|False|...podètz far lo vòt que volètz, l'exauçarem, qual que siá !
Pepper|4|False|Vòli... vòli... enfin... çò que volriái, seriá de
Carròt|5|True|Miau !
Carròt|6|False|Zzzzz

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|True|...mas ça que la !
Pepper|1|True|Sabi plan que de còps lo vòt melhor es que res cambie pas...
Pepper|3|False|« desirar tornar dormir » ? Qual gaspilhatge !
Carròt|4|True|Zzzzz
Carròt|5|False|Zzzzz
Crèdits|8|False|Abril de 2015 - www.peppercarrot.com - Dessenh e Scenari : David Revoy , correccions : Aurélien Gâteau
Narrator|6|False|Episòdi 7 : lo vòt
Narrator|7|False|FIN

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|1|False|Pepper&Carrot es completament liure, open source, e esponsorizat mercés al mecenat dels lectors. Per aqueste episòdi, mercés als 273 Mecènas :
Crèdits|4|False|https://www.patreon.com/davidrevoy
Crèdits|3|True|Vos tanben, venètz mecèna de Pepper&Carrot per l'episòdi venent :
Crèdits|7|False|Otisses : Aqueste episòdi foguèt dessenhat a 100% amb de logicials liures Krita sus Linux Mint
Crèdits|6|False|Open source : totas las sorsas, polissas d'escrituras, fichièrs amb calques son disponibles sul site oficial al telecargament.
Crèdits|5|False|Licéncia : Creative Commons Attribution podètz modificar, tornar partejar, vendre, etc...
Crèdits|2|False|Addison Lewis ❀ A Distinguished Robot ❀ Adrian Lord ❀ Ahmad Ali ❀ Aina Reich ❀ Alandran ❀ Alan Hardman ❀ Albert Westra ❀ Alcide Alex ❀ Alexander Bülow Tomassen ❀ Alexander Sopicki ❀ Alexandra Jordan ❀ Alexey Golubev ❀ Alex Flores ❀ Alex Lusco ❀ Alex Silver Alex Vandiver ❀ Alfredo ❀ Ali Poulton (Aunty Pol) ❀ Allan Zieser ❀ Andreas Rieger ❀ Andreas Ulmer ❀ Andrej Kwadrin ❀ Andrew Andrew Godfrey ❀ Andrey Alekseenko ❀ Angela K ❀ Anna Orlova ❀ anonymous ❀ Antan Karmola ❀ Anthony Edlin ❀ Antoine Antonio Mendoza ❀ Antonio Parisi ❀ Ardash Crowfoot ❀ Arjun Chennu ❀ Arne Brix ❀ Arturo J. Pérez ❀ Aslak Kjølås-Sæverud Axel Bordelon ❀ Axel Philipsenburg ❀ barbix ❀ BataMoth ❀ Ben Evans ❀ Bernd ❀ Betsy Luntao ❀ Birger Tuer Thorvaldsen Boonsak Watanavisit ❀ Boris Fauret ❀ Boudewijn Rempt ❀ BoxBoy ❀ Brent Houghton ❀ Brett Smith ❀ Brian Behnke ❀ Bryan Butler BS ❀ Bui Dang Hai Trieu ❀ BXS ❀ carlos levischi ❀ Carola Hänggi ❀ Cedric Wohlleber ❀ Charlotte Lacombe-bar ❀ Chris Radcliff Chris Sakkas ❀ Christian Gruenwaldner ❀ Christophe Carré ❀ Christopher Bates ❀ Clara Dexter ❀ codl ❀ Colby Driedger Conway Scott Smith ❀ Coppin Olivier ❀ Cuthbert Williams ❀ Cyol ❀ Cyrille Largillier ❀ Cyril Paciullo ❀ Damien ❀ Daniel Daniel Björkman ❀ Danny Grimm ❀ David ❀ David Tang ❀ DiCola Jamn ❀ Dmitry ❀ Donald Hayward ❀ Duke ❀ Eitan Goldshtrom Enrico Billich ❀ epsilon ❀ Eric Schulz ❀ Faolan Grady ❀ Francois Schnell ❀ freecultureftw ❀ Garret Patterson ❀ Ginny Hendricks GreenAngel5 ❀ Grigory Petrov ❀ G. S. Davis ❀ Guillaume ❀ Guillaume Ballue ❀ Gustav Strömbom ❀ Happy Mimic ❀ Helmar Suschka Henning Döscher ❀ Henry Ståhle ❀ Ilyas ❀ Irina Rempt ❀ Ivan Korotkov ❀ James Frazier ❀ Jamie Sutherland ❀ Janusz ❀ Jared Tritsch JDB ❀ Jean-Baptiste Hebbrecht ❀ Jean-Gabriel LOQUET ❀ Jeffrey Schneider ❀ Jessey Wright ❀ Jim ❀ Jim Street ❀ Jiska JoÃ£o Luiz Machado Junior ❀ Joerg Raidt ❀ Joern Konopka ❀ joe rutledge ❀ John ❀ John ❀ John Urquhart Ferguson ❀ Jónatan Nilsson Jonathan Leroy ❀ Jonathan Ringstad ❀ Jon Brake ❀ Jorge Bernal ❀ Joseph Bowman ❀ Juju Mendivil ❀ Julien Duroure ❀ Justus Kat Kai-Ting (Danil) Ko ❀ Kasper Hansen ❀ Kate ❀ Kathryn Wuerstl ❀ Ken Mingyuan Xia ❀ Kingsquee ❀ Kroet ❀ Lars Ivar Igesund Levi Kornelsen ❀ Liang ❀ Liselle ❀ Lise-Lotte Pesonen ❀ Lorentz Grip ❀ Louis Yung ❀ L S ❀ Luc Stepniewski ❀ Luke Hochrein ❀ MacCoy Magnus Kronnäs ❀ Manuel ❀ Manu Järvinen ❀ Marc & Rick ❀ marcus ❀ Martin Owens ❀ Mary Brownlee ❀ Masked Admirer Mathias Stærk ❀ mefflin ross bullis-bates ❀ Michael ❀ Michael Gill ❀ Michael Pureka ❀ Michelle Pereira Garcia ❀ Mike Mosher Miroslav ❀ mjkj ❀ Nazhif ❀ Nicholas DeLateur ❀ Nicholas Terranova ❀ Nicki Aya ❀ Nicola Angel ❀ Nicolae Berbece ❀ Nicole Heersema Nielas Sinclair ❀ NinjaKnight Comics ❀ Noble Hays ❀ Noelia Calles Marcos ❀ Nora Czaykowski ❀ No Reward ❀ Nyx ❀ Olivier Amrein Olivier Brun ❀ Olivier Gavrois ❀ Omar Willey ❀ Oscar Moreno ❀ Öykü Su Gürler ❀ Ozone S. ❀ Pablo Lopez Soriano ❀ Pat David Patrick Gamblin ❀ Paul ❀ Paul ❀ Pavel Semenov ❀ Pet0r ❀ Peter ❀ Peter Moonen ❀ Petr Vlašic ❀ Philippe Jean Edward Bateman Pierre Geier ❀ Pierre Vuillemin ❀ Pranab Shenoy ❀ Pyves & Ran ❀ Raghavendra Kamath ❀ Rajul Gupta ❀ Reorx Meng ❀ Ret Samys rictic ❀ RJ van der Weide ❀ Roberto Zaghis ❀ Robin Moussu ❀ Roman Burdun ❀ Rumiko Hoshino ❀ Rustin Simons ❀ Sally Bridgewater Sami T ❀ Samuel Mitson ❀ Scott Petrovic ❀ Sean Adams ❀ Shadefalcon ❀ ShadowMist ❀ shafak ❀ Shawn Meyer ❀ Simon Forster Simon Isenberg ❀ Sonja Reimann-Klieber ❀ Sonny W. ❀ Soriac ❀ Stanislav Vodetskyi ❀ Stephanie Cheshire ❀ Stephen Bates Stephen Smoogen ❀ Steven Bennett ❀ Stuart Dickson ❀ surt ❀ Sybille Marchetto ❀ TamaskanLEM ❀ tar8156 ❀ Terry Hancock TheFaico ❀ thibhul ❀ Thomas Citharel ❀ Thomas Courbon ❀ Thomas Schwery ❀ Thornae ❀ Tim Burbank ❀ Tim J. ❀ Tomas Hajek Tom Demian ❀ Tom Savage ❀ Tracey Reuben ❀ Travis Humble ❀ tree ❀ Tyson Tan ❀ Urm ❀ Victoria ❀ Victoria White Vladislav Kurdyukov ❀ Vlad Tomash ❀ WakoTabacco ❀ Wander ❀ Westen Curry ❀ Witt N. Vest ❀ WoodChi ❀ Xavier Claude Yalyn Vinkindo ❀ Yaroslav ❀ Zeni Pong ❀ Źmicier Kušnaroŭ ❀ Глеб Бузало ❀ 獨孤欣 & 獨弧悦
