# Transcript of Pepper&Carrot Episode 32 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 32: Het slagveld

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koning|1|True|Officier!
Koning|2|False|Heb je een heks in dienst genomen zoals ik je heb opgedragen?
Officier|3|True|Ja, Majesteit!
Officier|4|False|Ze staat daar, aan Uw andere zijde.
Koning|5|False|...?
Pepper|6|True|Hoi hoi!
Pepper|7|False|Mijn naam is Pepp...
Koning|8|False|?!!
Koning|9|True|Stuk onbenul!!!
Koning|10|True|Waarom heb jij dit meisje aangenomen?!
Koning|11|False|Ik heb een échte strijdmagiër nodig!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Uh, pardon?!
Pepper|2|True|Ik ben een echte heks van Chaosah.
Pepper|3|False|Ik heb zelfs een diploma dat bewij...
Geschrift|4|True|Diploma
Geschrift|5|True|van
Geschrift|6|False|Chaosah
Geschrift|8|True|Cayenne
Geschrift|9|False|Cumin
Geschrift|7|True|T h ym|nowhitespace
Geschrift|10|False|~ voor Pepper ~
Koning|11|False|Zwijg!
Koning|12|True|Ik heb geen behoefte aan een klein meisje in dit leger.
Koning|13|False|Keer terug naar huis en ga daar met je poppen spelen.
Geluid|14|False|Pats!
Leger|15|True|HAHA HA HA !
Leger|16|True|HAHA HA HA !
Leger|17|True|HAHA HA HA !
Leger|18|False|HA HA HAHA !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Niet te geloven!
Pepper|2|False|Al die jaren studie en niemand die me serieus neemt met als excuus...
Pepper|3|False|...dat ik er onervaring uit zou zien!
Geluid|4|False|Bam ! !|nowhitespace
Pepper|5|False|CARROT!|nowhitespace
Geluid|6|False|PAF ! !|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Serieus, Carrot?
Pepper|2|True|Ik hoop dat dit maaltje de moeite waard was.
Pepper|3|False|Je ziet er afgrijselijk uit...
Pepper|4|False|... Je ziet er...
Pepper|5|True|Beeldvorming!
Pepper|6|False|Natuurlijk!
Geluid|7|False|Krak !|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Hé!
Pepper|2|False|Het is mij ter ore gekomen dat U een ECHTE HEKS zocht?!?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koning|1|True|Laten we toch dat meisje maar terugroepen.
Koning|2|False|Deze kost vast véél te veel.
Geschrift|3|False|Wordt vervolgd...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Jij kan ook patroon van Pepper&Carrot worden en dan komt jouw naam hierbij!
Pepper|3|True|Pepper&Carrot is helemaal vrij,open-bron en gesponsord door de giften van haar lezers.
Pepper|4|False|Voor deze aflevering bedank ik de 1121 patronen!
Pepper|7|True|Kijk op www.peppercarrot.com voor alle info!
Pepper|6|True|Je kan doneren met Patreon, Tipeee, PayPal, Liberapay ... en meer!
Pepper|8|False|Dank je!
Pepper|2|True|Wist je dat?
Aftiteling|1|False|31 maart 2020 Tekeningen & verhaal: David Revoy. Bèta-feedback: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Parnikkapore, Stephen Paul Weber, Valvin, Vejvej. Nederlandse versie Vertaling: Julien Bertholon. Proeflezing: Marno van der Maas. Gebaseerd op het Hereva-universum Bedenker: David Revoy. Hoofdonderhouder: Craig Maloney. Schrijvers: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Verbeteraars : Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.2.9-beta, Inkscape 0.92.3 op Kubuntu 19.10. Licentie: Creative Commons Naamsvermelding 4.0. www.peppercarrot.com
<hidden>|9|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|10|False|You can also translate this page if you want.
<hidden>|11|False|Beta readers help with the story, proofreaders give feedback about the text.
