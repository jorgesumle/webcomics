# Transcript of Pepper&Carrot Episode 11 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 11 : Las mascas de Caosah

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Pepper, fas vergonha a Caosah.
Frigola|2|False|Cayenne a rason ; una masca de Caosah coma cal deu provocar la paur, l'obesissença e lo respècte.
Comin|3|False|...e tu, ofrisses de còcas e de tè a nòstres quites demònis*...
Nòta|4|False|* véser episòdi 8: L'aniversari de Pepper.
Pepper|5|True|Mas...
Pepper|6|False|...mairinas...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|4|False|CALA-TE !
Comin|9|False|Tadam !
Frigola|1|True|Pepper, siás dotada, rai, mas siás tanben la sola a nos succedir.
Frigola|2|False|Es nòstre dever de far de tu una vertadièra masca marrida de Caosah.
Pepper|3|False|Mas... vòli pas èstre marrida ! Es... va al revèrs de ma....
Cayenne|5|True|...o tornarem préner totes tos poders !
Cayenne|6|False|E tornaràs èstre la petita piòta orfanèla del Cap de l'Esquiròl.
Frigola|7|False|Comin t'acompanharà de'n pertot, ara, per te formar e nos assegurar que te las viras plan.
Son|8|False|Pof!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Comin|1|False|Ta primièra mission es d'anar espaurir aquel monarque per lo manipular.
Comin|2|False|Una masca vertadièra de Caosah a d'influéncia suls potents.
Comin|3|False|Lo Rei novèl d'Acren es tròp doç e brave amb sos subjèctes. Lo pòble deu aver paur de son rei.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Lo Rei ?...
Pepper|2|False|...tan jove ?!
Pepper|3|False|Deu èstre de mon temps...
Comin|4|False|Mas de qu'espèras ! Vai-z-i ! Desvelha-lo, sermona-lo, fai-li paur !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Tu e ieu sèm quasi parions...
Pepper|3|True|joves...
Pepper|4|True|sols...
Pepper|5|False|...presonièrs de nòstres destins.
Son|1|False|Dzzz

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Frigola|6|False|Avèm fòrça trabalh amb tu, Pepper...
Crèdits|8|False|09/2015 - Dessenh & Scenari : David Revoy
Narrator|7|False|- FIN -
Pepper|2|False|?!
Frigola|4|True|Primièr tèst :
Frigola|5|False|FRACASSÀS !
Son|1|False|Pof !
Son|3|False|Fluf !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|1|False|Pepper&Carrot es completament liure, open source, e esponsorizat mercés al mecenat de sos lectors. Per aqueste episòdi, mercé als 502 mecènas :
Crèdits|3|False|https://www.patreon.com/davidrevoy
Crèdits|2|True|Vos tanben, venètz mecèna de Pepper&Carrot per l'episòdi venent sus
Crèdits|4|False|Licéncia : Creative Commons Attribution 4.0 Sorsas : disponiblas sus www.peppercarrot.com Logicials : aqueste episòdi foguèt dessenhat a 100% amb de logicials liures Krita 2.9.6, G'MIC 1.6.5.2, Inkscape 0.91 sus Linux Mint 17
