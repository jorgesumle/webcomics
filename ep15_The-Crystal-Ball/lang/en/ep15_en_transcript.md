# Transcript of Pepper&Carrot Episode 15 [en]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episode 15: The Crystal Ball

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|- FIN -
Credits|2|False|03/2016 - Art & Scenario : David Revoy

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot is entirely free(libre), open-source and sponsored thanks to the patronage of its readers. For this episode, thank you to the 686 Patrons:
Credits|2|True|You too can become a patron of Pepper&Carrot for the next episode at
Credits|3|False|https://www.patreon.com/davidrevoy
Credits|4|False|License : Creative Commons Attribution 4.0 Source : available at www.peppercarrot.com Software : this episode was 100% drawn with libre software Krita 2.9.11, Inkscape 0.91 on Linux Mint 17
