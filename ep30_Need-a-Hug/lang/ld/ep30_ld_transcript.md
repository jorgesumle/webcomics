# Transcript of Pepper&Carrot Episode 30 [ld]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zha wudetha|1|False|Wud 30: Lemadamath Themeshub

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Dedidehá|1|False|- NODI -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Bóo naden ne Loyud i Ámed beth loshenan i thel ne zha nethath nuha !
<hidden>|0|False|Bíi dínonehóo Loyud i Ámed wa. Dínon woban betha. Meden wéedaná i meban ben wothem wolotheth.
<hidden>|0|False|Áala withedim 973 wudewan hi!
Dohiná|1|False|Bóo láad www.peppercarrot.com beth lothewan!
Loyud|2|True|Meham lezh Patreon, Tipeee, PayPal, Liberapay bezheha ...i nidi nedebe!
Loyud|3|True|Áala!
Loyud|4|False|Báa lothel le?
Loyud|5|True|September 03, 2019 Alehala i woban: David Revoy. Wodide wowéedan dedidethu: Alina the Hedgehog, Craig Maloney, Jihoon Kim, Parnikkapore, Martin Disch, Nicolas Artance, Valvin. Láadan Héedan Láadanedim: Yuli i Álo . Bíi nosháad dedide thera Shebedoni bethude wa Elá : David Revoy. Wohun wonayahá: Craig Maloney. Dedidethodá: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Dódóoná: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Bodibod: Krita/4.2~git branch, Inkscape 0.92.3, Kubuntu 18.04.2 beha. Hudi: Creative Commons Attribution 4.0. www.peppercarrot.com
Loyud|6|True|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
Loyud|7|True|You can also translate this page if you want.
Loyud|8|False|Beta readers help with the story, proofreaders give feedback about the text.
