# Transcript of Pepper&Carrot Episode 26 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 26 : Los libres, aquò es genial

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Los libres, aquò es genial !
Pepper|3|False|Es una vertadièra meravilha !
Pepper|2|True|Dins aqueste libre rare, i a las nòtas d'un aventurièr experimentat, e descriu tot dins aquel donjon.
Pepper|4|False|Per exemple, l'uèlh malefic de la pòrta principala lança de bòlas de fuòc per rebutar los intruses.
Pepper|5|False|Ça que la, se nos aprocham sul costat, fòra vista…

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|…e que cobrissèm l'uèlh amb un capèl…
Pepper|3|False|…la pòrta se deuriá dobrir d'esperela !
Pepper|4|True|A mai, conselhan de daissar lo capèl aquí per se poder escapar aisidament !
Pepper|5|False|Los libres es tras que genial !
Escritura|6|False|« Sautatz sul pic per-dessús lo primièr balcon . »
Pepper|7|False|Zo !
Son|9|False|BOING
Son|10|False|BOING
Escritura|8|False|« Notaratz un sentit de casuda. Es normal. Es una travèrsa. »
Escritura|11|False|« Profitatz del confòrt de la tela illuminada sens cap de risca : aquelas aranhas fotosinteticas se noirisson pas que del lum que rebaton. »
Escritura|12|False|« Recomandi una prangièra rapida per recobrar totas las fòrças. »
Pepper|13|False|Aimi los libres !
Son|2|False|Pof !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Sabi quant los gats detèstan èstre trempats.
Pepper|3|True|Te'n fagas pas !
Pepper|4|False|Los libres son tanben utiles per aquò !
Pepper|5|True|E mens de dètz minutas après èstre dintrada dins lo donjon, aquí soi dins la sala del tresaur a culhir las fuèlhas magicas del famós Arbre d'Aiga.
Pepper|6|False|Los libres, aquò es magic !
Pepper|1|True|Òu, Carròt…

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Escritura|3|False|« Coma avètz pres una travèrsa, deuriatz arribar a la sala del Gardian. »
Pepper|4|True|Mmm…
Pepper|5|True|Lo boss final ?!
Pepper|6|False|Mas ont ?
Pepper|7|True|HA-HA !
Pepper|8|False|Trobat !
Pepper|11|True|Pffff... !
Pepper|12|False|Es tròp aisit quand coneissèm son feble !
Escritura|1|False|« Amassatz una pluma en passant. »
Pepper|2|False|Manca pas de plumas, per aquí !
Pepper|9|True|gratilh
Pepper|10|False|gratilh

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Son|1|False|Crrrr !|nowhitespace
Pepper|2|False|Aha ! Aquí la sortida !
Son|3|False|Flooop !
Pepper|5|True|Bèèèè…
Pepper|6|False|Soi segura qu'i a d'instruccions per tirar aquesta « causa »…
Pepper|7|True|Carròt… me pòs legir aquò ?
Pepper|8|True|Èèè… solide que non…
Pepper|9|False|Pff…
Pepper|4|False|?!
Pepper|12|False|Grrrr !!!
Pepper|10|True|O SABIÁI !!!
Pepper|11|True|Èra tròp plan per èstre vertat !
Pepper|13|False|Benlèu que los libres pòdon pas tot fixar, en fait…
Son|14|False|Plac !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|…levat se…
Pepper|2|False|YAA A A A A A A ! ! !|nowhitespace
Son|3|False|POF !|nowhitespace
Pepper|5|False|…los libres, aquò es genial !
Pepper|4|True|E non, en fait, dins totas las situacions…
Narrator|6|False|- FIN -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|3|False|Vos tanben, venètz mecèna de Pepper&Carrot sus www.patreon.com/davidrevoy
Crèdits|2|False|Pepper&Carrot es completament liure, open source, e esponsorizat mercés al mecenat de sos lectors. Per aqueste episòdi, mercé als 1098 mecènes :
Crèdits|1|False|Licéncia : Creative Commons Attribution 4.0. Logicials : Krita 4.1, Inkscape 0.92.3 sus Kubuntu 17.10. Dessenh & Scenari : David Revoy. Script doctor : Craig Maloney. Correctors : Craig Maloney, CalimeroTeknik. Beta-lectors : Ambernite, Craig Maloney, CalimeroTeknik, Jookia, Midgard, Nartance, Popolon, Quiralta, Valvin, xHire, Zeograd. Version Occitana (lengadocian) Traductor : Aure Séguier. Basat sus l'univèrs d'Hereva Creacion : David Revoy. Mantenença : Craig Maloney. Editors : Craig Maloney, Nartance, Scribblemaniac, Valvin. Correctors : Willem Sonke, Moini, Hali, CGand, Alex Gryson. 28 de julhet de 2018 www.peppercarrot.com
<hidden>|0|False|Remove the names of the people who helped to create the current version and create this section for your language. You can credit translators, proofreaders, whatever categories you want. N.B.: In Inkscape, if you press Enter to create a new line, it will not push down text below it until the line has text. So to create a whiteline, type a space on its own line.
