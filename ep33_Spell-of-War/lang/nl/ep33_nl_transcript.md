# Transcript of Pepper&Carrot Episode 33 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 33: De ban des oorlogs

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vijand|1|False|ahWOEEEEEEEEEE !
Leger|2|False|Roooooar !!
Leger|5|False|Grroooaarr !!
Leger|4|False|Grrrr !
Leger|3|False|Yuurrr !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koning|1|True|Oké, jonge heks.
Koning|2|False|Als je een spreuk kent die ons helpen kan, dan is het nu of nooit!
Pepper|3|True|Begrepen!
Pepper|4|False|Zet U schrap en aanschouw...
Geluid|5|False|Dzjiii! !|nowhitespace
Pepper|6|False|...MIJN MEESTERWERK!!
Geluid|7|False|Dzjiiii! !|nowhitespace
Pepper|8|False|Realitas Hackeris Pepperus!
Geluid|9|False|Dzzjooo! !|nowhitespace

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|Fiitz!
Geluid|2|False|Dzii!
Geluid|3|False|Tshii!
Geluid|4|False|Ffhii!
Geluid|8|False|Dziing!
Geluid|7|False|Fiitz!
Geluid|6|False|Tshii!
Geluid|5|False|Ffhii!
Koning|9|True|Is dit een spreuk die onze zwaarden sterker maakt...
Koning|10|False|...en de wapens van onze vijanden zwakker?
Geluid|11|False|Dzii...
Pepper|12|True|Hehe.
Pepper|13|True|U zult het zien!
Pepper|14|False|Het enige wat ik kan zeggen, is dat U vandaag geen enkele soldaat verliest.
Pepper|15|False|Maar niet zonder slag of stoot, en niet zonder jullie volle inzet!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koning|1|False|Dat is precies waarvoor we hebben geoefend!
Koning|2|False|OP MIJN BEVEL!
Leger|3|False|Jaaa!
Leger|4|False|Waaah!
Leger|5|False|Jaaa!
Leger|6|False|Jaaa!
Leger|7|False|Waaaah!
Leger|8|False|Jaaah!
Leger|9|False|Yihaaa !
Leger|10|False|Jahii!
Leger|11|False|Jaa!
Koning|12|False|Aaaaaanvalluuuuuuuuh!!!
Leger|13|False|Waaaaah!
Leger|14|False|Jaaa!
Leger|15|False|Yihaaa!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koning|1|False|Jaaaa!!
Vijand|2|False|Whaaah!!!
Geluid|3|False|Tshiiiing! !|nowhitespace
Geluid|4|False|Swoosh! !|nowhitespace
Geschrift|5|False|12
Vijand|6|False|?!!
Geschrift|7|False|8
Koning|8|False|?!!
Geschrift|9|False|64
Geschrift|10|False|32
Geschrift|11|False|72
Geschrift|12|False|0
Geschrift|13|False|64
Geschrift|14|False|0
Geschrift|15|False|56
Leger|20|False|Yrrr !
Leger|17|False|Yurr !
Leger|19|False|Grrr !
Leger|21|False|Yaaa !
Leger|18|False|Ouaaais !
Leger|16|False|Yâaa !
Geluid|27|False|Sshing
Geluid|25|False|Fffchh
Geluid|22|False|wiizz
Geluid|23|False|Swoosh
Geluid|24|False|Chklong
Geluid|26|False|Chkilng

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koning|1|False|?!
Geschrift|2|False|3
Geschrift|3|False|24
Geschrift|4|False|38
Geschrift|5|False|6
Geschrift|6|False|12
Geschrift|7|False|0
Geschrift|8|False|5
Geschrift|9|False|0
Geschrift|10|False|37
Geschrift|11|False|21
Geschrift|12|False|62
Geschrift|13|False|27
Geschrift|14|False|4
Koning|15|False|! !|nowhitespace
Koning|16|False|HÈÈÈÈÈKS! WAT HEB JIJ UITGESPOOKT?!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Tadaaa!
Pepper|2|True|"Dit"
Pepper|3|False|...is mijn meesterwerk!
Pepper|4|False|Het is een complexe bezwering die de realiteit verandert en het aantal overgebleven levenspunten van je tegenstander laat zien.
Geschrift|5|False|33
Pepper|6|True|Wanneer je levenspunten op zijn, moet je het slagveld verlaten tot het einde van het gevecht.
Pepper|7|True|Het eerste leger dat alle tegenstanders van het veld verwijdert, wint!
Pepper|8|False|Doodsimpel.
Geschrift|9|False|0
Leger|10|False|?
Pepper|11|True|Geweldig toch, of niet?
Pepper|12|True|Geen doden meer!
Pepper|13|True|Geen gewonde soldaten meer!
Pepper|14|False|Dit gaat oorlogsvoering revolutioneren!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Nu dat jullie de regels kennen, zal ik alle tellers resetten en dan kunnen we overnieuw beginnen.
Pepper|2|False|Dat is wel zo eerlijk.
Geschrift|3|False|64
Geschrift|4|False|45
Geschrift|5|False|6
Geschrift|6|False|2
Geschrift|7|False|0
Geschrift|8|False|0
Geschrift|9|False|0
Geschrift|10|False|0
Geschrift|11|False|9
Geschrift|12|False|5
Geschrift|13|False|0
Geschrift|14|False|0
Geschrift|15|False|0
Geluid|16|False|Sshing!
Geluid|17|False|Zouu!
Geluid|19|False|Tjak!
Geluid|18|False|Bonk!
Pepper|20|True|Ren sneller, Carrot!
Pepper|21|False|De spreuk zal niet lang meer blijven werken!
Verteller|22|False|- Einde -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|1|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|2|False|You can also translate this page if you want.
<hidden>|3|False|Beta readers help with the story, proofreaders give feedback about the text.
Aftiteling|4|False|29 juni 2020 Tekeningen & verhaal: David Revoy. Bèta-feedback: Arlo James Barnes, Craig Maloney, GunChleoc, Hyperbolic Pain, Karl Ove Hufthammer, Martin Disch, Nicolas Artance, Quetzal2, Valvin. Nederlandse versie Vertaling: Julien Bertholon. Proeflezing: Marno van der Maas. Gebaseerd op het Hereva-universum Bedenker: David Revoy. Hoofdonderhouder: Craig Maloney. Schrijvers: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Verbeteraars: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.3, Inkscape 1.0 op Kubuntu 19.10. Licentie : Creative Commons Naamsvermelding 4.0. www.peppercarrot.com
Pepper|5|True|Wist je dat?
Pepper|6|True|Pepper&Carrot is helemaal vrij,open-bron en gesponsord door de giften van haar lezers.
Pepper|7|False|Voor deze aflevering bedank ik de 1190 patronen!
Pepper|8|True|Jij kan ook patroon van Pepper&Carrot worden en dan komt jouw naam hierbij!
Pepper|9|True|Je kan doneren met Patreon, Tipeee, PayPal, Liberapay ... en meer!
Pepper|10|True|Kijk op www.peppercarrot.com voor alle info!
Pepper|11|False|Dank je!
