# Transcript of Pepper&Carrot Episode 06 [sr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Епизода 6: Такмичење у прављењу напитака
<hidden>|2|False|Папричица
<hidden>|3|False|Шаргарепица

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Дођавола, поново сам заспала са отвореним прозором...
Pepper|2|True|... тако је ветровито...
Pepper|3|False|... и зашто могу да видим Комону кроз прозор?
Pepper|4|False|КОМОНА!!!
Pepper|5|False|Напитак! Такмичење!!!
Pepper|6|True|Мора да сам... Мора да сам се случајно успавала
Pepper|9|True|... али?
Pepper|10|False|Где сам то ја?!?
Bird|12|False|а?|nowhitespace
Bird|11|True|кв|nowhitespace
Pepper|7|False|*
Note|8|False|* Види епизоду 4: Напад генијалности

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Шаргарепице! Баш лепо од тебе што си се сетио да ме понесеш на такмичење!
Pepper|3|False|Фан-та-сти-чно!
Pepper|4|True|Сетио си се чак да понесеш и напитак, и моју одећу, и шешир...
Pepper|5|False|... да видим који си напитак понео...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|ШТА ?!?
Mayor of Komona|3|False|Као градоначелник Комоне, проглашавам такмичење у прављењу напитака отвореним!
Mayor of Komona|4|False|Нашем граду је велика част да поздрави четири младе вештице за ово прво издање и пожели им добродошлицу
Mayor of Komona|5|True|Молим вас, један
Mayor of Komona|6|True|велики
Writing|2|False|Такмичење у Комони: Напици
Mayor of Komona|7|False|аплауз!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Клеп
Mayor of Komona|1|True|Из далеке Уније технолога, имам част да пожелим добродошлицу заносној и генијалној
Mayor of Komona|3|True|... не треба заборавити нашу мештанку, личну вештицу Комоне,
Mayor of Komona|5|True|... Наша трећа учесница нам долази из земаља залазећих месеца,
Mayor of Komona|7|True|... и на крају, наша последња учесница, из шуме Веверичјег Кутка,
Mayor of Komona|2|False|Коријандри !
Mayor of Komona|4|False|Шафран!
Mayor of Komona|6|False|Шикими!
Mayor of Komona|8|False|Папричица!
Mayor of Komona|9|True|Нека игре почну!
Mayor of Komona|10|False|Гласаће се аплаузима!
Mayor of Komona|11|False|Прва на реду је Коријандра...
Coriander|13|False|... смрти се више не треба плашити, захваљујући мом...
Coriander|14|True|... напитку
Coriander|15|False|ЗОМБИФИКАЦИЈЕ !
Audience|16|True|Клеп
Audience|17|True|Клеп
Audience|18|True|Клеп
Audience|19|True|Клеп
Audience|20|True|Клеп
Audience|21|True|Клеп
Audience|22|True|Клеп
Audience|23|True|Клеп
Audience|24|True|Клеп
Audience|25|True|Клеп
Audience|26|True|Клеп
Audience|27|True|Клеп
Audience|28|True|Клеп
Coriander|12|False|Даме и господо...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|ФАНТАСТИЧНО!
Audience|3|True|Тап
Audience|4|True|Тап
Audience|5|True|Тап
Audience|6|True|Тап
Audience|7|True|Тап
Audience|8|True|Тап
Audience|9|True|Тап
Audience|10|True|Тап
Audience|11|True|Тап
Audience|12|True|Тап
Audience|13|True|Тап
Audience|14|True|Тап
Audience|15|True|Тап
Audience|16|False|Тап
Saffron|18|True|зато што је на реду
Saffron|17|True|... али молим вас, сачувајте свој аплауз, народе Комоне,
Saffron|22|False|... и због кога ће вам завидети!
Saffron|19|True|МОЈ
Saffron|25|False|ОТМЕНОСТИ !
Saffron|24|True|... напитка
Saffron|23|False|... све ово је могуће уз просту примену једне капи мог...
Audience|26|True|Клеп
Audience|27|True|Клеп
Audience|28|True|Клеп
Audience|29|True|Клеп
Audience|30|True|Клеп
Audience|31|True|Клеп
Audience|32|True|Клеп
Audience|33|True|Клеп
Audience|34|True|Клеп
Audience|35|True|Клеп
Audience|36|True|Клеп
Audience|37|True|Клеп
Audience|38|True|Клеп
Audience|39|True|Клеп
Audience|40|False|Клеп
Mayor of Komona|42|False|Овај напитак може обогатити целу Комону!
Mayor of Komona|41|True|Фантастично! Невероватно!
Audience|44|True|Тап
Audience|45|True|Тап
Audience|46|True|Тап
Audience|47|True|Тап
Audience|48|True|Тап
Audience|49|True|Тап
Audience|50|True|Тап
Audience|51|True|Тап
Audience|52|True|Тап
Audience|53|True|Тап
Audience|54|True|Тап
Audience|55|True|Тап
Audience|56|True|Тап
Audience|57|True|Тап
Audience|58|True|Тап
Audience|59|True|Тап
Audience|60|True|Тап
Mayor of Komona|2|False|Коријандра пркоси смрти овим чудесним напитком!
Saffron|21|True|Прави напитак, који сте сви ви чекали: напитак, који ће задивити ваше комшије...
Mayor of Komona|43|False|Ваш аплауз не греши. Коријандра је већ елиминисана
Audience|61|False|Тап
Saffron|20|False|напитак

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Чини се да ће последњу демонстрацију Шикими тешко надмашити
Shichimi|4|True|НЕ!
Shichimi|5|True|Не смем, исувише је опасно
Shichimi|6|False|ИЗВИНИТЕ!
Mayor of Komona|3|False|... хајде, Шикими, сви на тебе чекају...
Mayor of Komona|7|False|Изгледа, даме и господо, да Шикими губи право...
Saffron|8|False|Дај то овамо!
Saffron|9|False|... и престани да се претвараш како си стидљива, квариш забаву
Saffron|10|False|Сви већ знају да сам ја победила, независно од тога шта напитак ради...
Shichimi|11|False|!!!
Sound|12|False|ЗЗ З З УУ УУУП|nowhitespace
Shichimi|15|False|ОГРОМНА ЧУДОВИШТА!!!
Shichimi|2|False|Ја... Нисам знала да ће бити демонстрације
Shichimi|13|True|ПАЗИ!!!
Shichimi|14|True|То је напитак за

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|КУ-КУ-Ри ии ку уу уу у у у|nowhitespace
Sound|2|False|БАМ!
Pepper|3|True|... ха, супер!
Pepper|5|False|...мој напитак ће макар изазвати смех зато што...
Pepper|4|False|јесам ли сада ја на реду?
Mayor of Komona|6|True|Трчи, будало!
Mayor of Komona|7|False|Такмичење је готово! ...спашавај се!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|... као и обично, сви оду када смо ми на реду
Pepper|1|True|ето видиш...
Pepper|4|True|Макар имам идеју шта могу да урадим са твојим "напитком", Шаргарепице
Pepper|5|False|... да вратим све на своје место и одем кући!
Pepper|7|True|Ти
Pepper|8|False|Превелики-отмени-зомби-канаринцу!
Pepper|10|False|Хоћеш да пробаш и последњи напитак?
Pepper|11|False|... не баш, а?
Pepper|6|False|ХЕЈ!
Sound|9|False|К Р РР ц !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Тако је, пажљиво прочитај шта пише на етикети
Pepper|2|False|... Нећу се двоумити да ли да ти сипам на главу или не уколико не напустиш Комону истог трена!
Mayor of Komona|3|True|Зато што је спасила наш град када је био у опасности
Mayor of Komona|4|False|Награда за прво место иде Папричици и њеном напитку... ?!
Pepper|7|False|... па... у ствари ово и није напитак; ово је узорак урина мог мачка са његове задње посете ветеринару
Pepper|6|True|... Хаха! да ...
Pepper|8|False|... значи без демонстрације?..
Narrator|9|False|Епизода 6: Такмичење у прављењу напитака
Narrator|10|False|КРАЈ
Credits|11|False|Март 2015 - Графика and прича David Revoy - Превод Милена Петровић

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot је у потпуности бесплатан стрип отвореног кода који је под покровитељством читалаца. За ову епизоду је заслужно 245 покровитеља:
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|И ти можеш да постанеш покровитељ следеће епизоде Папричице и Шаргарепице:
Credits|7|False|Алати : Ова епизода је 100% производ бесплатног/слободног софтвера Krita на Linux Mint
Credits|6|False|Отворен код: све изворне датотеке са фонтовима и слојевима су доступне на званичној страници.
Credits|5|False|Лиценца : Creative Commons Attribution стрип се може мењати, објављивати, продавати итд.
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
