# Transcript of Pepper&Carrot Episode 28 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 28: Feiringa

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Forteljar|1|True|Den kvelden stod alle Herevas tre månar på linje ...
Forteljar|2|False|... og i gjenskinet var zombiah-katedralen eit mektig syn!
Forteljar|3|False|Det var under dette magiske lyset at veninna mi Koriander vart krona til ...
Forteljar|4|False|... dronning av Kvalistad.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Forteljar|1|False|Og etterpå var det feiring.
Forteljar|2|False|Eit storslege selskap – med alle trolldomsskulane, og hundrevis av viktige gjestar frå heile Hereva.
Pepar|3|False|!?

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Å, er det deg?
Pepar|3|False|Eg stod heilt i mine eigne tankar.
Gulrot|2|False|gni gni
Pepar|4|False|Eg går inn. Det byrjar å verta kaldt.
Journalist|6|False|Kom igjen! Der er ho!
Journalist|5|False|Skund dykk! No er ho her!
Journalist|7|False|Frøken Safran! Vil du dela nokre ord med lesarane av Kvalistad tidend?
Safran|8|False|Sjølvsagt det!
Pepar|15|False|...
Pepar|16|False|No trur eg eg skjønar kva som gjer at eg ikkje er heilt i feststemning, Gulrot.
Lyd|9|False|B LITS|nowhitespace
Journalist|13|True|Frøken Safran! Hereva moteblad her.
Lyd|10|False|B LITS|nowhitespace
Lyd|11|False|BLITS|nowhitespace
Lyd|12|False|B LITS|nowhitespace
Journalist|14|False|Kven er det som har designa kjolen din?

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Alle veninnene mine er så vellukka. Av og til skulle eg ynskja eg hadde berre ein brøkdel av det dei har.
Pepar|2|False|Koriander er dronning.
Pepar|3|False|Og Safran ei rik superstjerne.
Pepar|4|False|Også Shichimi ser så perfekt ut, der ho vandrar saman med skulen sin.
Pepar|5|False|Og eg? Kva har eg?
Pepar|6|False|Det her.

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|False|Pepar?
Shichimi|2|False|Eg sneik med meg litt mat. Lyst på?

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Shichimi|1|True|Det kjennest rart ut å måtta snika seg unna for å få ein matbit.
Shichimi|2|False|Men læraren vår seier at me må te oss som reine sjeler – utan teikn på at me har menneskelege behov.
Safran|3|False|Så det er her de er!
Safran|4|True|Endeleg! Ein stad eg kan sleppa unna alle fotografane!
Safran|5|False|Eg vert heilt sprø av dei!
Koriander|6|False|Fotografar?
Koriander|7|False|Prøv å sleppa unna endelause samtalar med keisame politikarar når du går rundt med ei slik ei på hovudet, du.
Lyd|8|False|klø, klø

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Koriander|1|True|Eg fekk forresten endeleg treft gudmødrene dine, Pepar.
Koriander|2|False|Dei er verkeleg noko for seg sjølve.
Pepar|3|False|!!!
Koriander|4|True|Altså, du er så heldig!
Koriander|5|False|Du får sikkert lov av dei til å gjera nett kva du vil.
Safran|7|False|Eller dansa og kosa deg utan å måtta bry deg om kva folk måtte meina!
Koriander|6|False|Som å droppa å vera overdrive høfleg utan å måtta frykta ei diplomatisk krise.
Shichimi|8|False|Eller prøva all maten på bufféen utan å vera uroa for at nokon skulle koma til å sjå deg!

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Forteljar|4|False|FRAMHALD FØLGJER ...
Shichimi|1|False|Oi! Pepar? Sa me noko gale? Er du lei deg?
Pepar|2|True|Slett ikkje!
Pepar|3|False|Tusen takk, gode vener!

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|Januar 2019 Teikningar og forteljing: David Revoy. Tidleg tilbakemelding: CalimeroTeknik, Craig Maloney, Martin Disch, Midgard, Nicolas Artance og Valvin. Korrekturlesing engelsk versjon: CalimeroTeknik, Craig Maloney og Martin Disch. Omsetjing til nynorsk : Karl Ove Hufthammer og Arild Torvund Olsen. Bygd på Hereva-universet Skapar: David Revoy. Hovudutviklar: Craig Maloney. Forfattarar: Craig Maloney, Nartance, Scribblemaniac og Valvin. Rettingar: Willem Sonke, Moini, Hali, CGand og Alex Gryson . Programvare: Krita 4.1.5~appimage og Inkscape 0.92.3 på Kubuntu 18.04.1. Lisens: Creative Commons Attribution 4.0. www.peppercarrot.com
Pepar|3|True|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane.
Pepar|4|False|Takk til dei 960 som støtta denne episoden!
Pepar|2|True|Visste du dette?
Pepar|5|True|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot og få namnet ditt her!
Pepar|7|True|Sjå www.peppercarrot.com for meir informasjon!
Pepar|6|True|Me er på blant anna Patreon, Tipeee, PayPal og Liberapay ...
Pepar|8|False|Tusen takk!
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
