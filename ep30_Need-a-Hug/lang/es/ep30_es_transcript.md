# Transcript of Pepper&Carrot Episode 30 [es]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Episodio 30: Necesito un abrazo

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrator|1|False|- FIN -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|¡Tú tabién puedes ser mecenas de Pepper&Carrot y ver tu nombre inscrito aquí!
Pepper|3|True|Pepper&Carrot es completamente libre, gratuito, de código abierto y financiado gracias al mecenazgo de sus lectores.
Pepper|4|False|¡Este episodio ha recibido el apoyo de 973 mecenas!
Pepper|7|True|¡Visítanos en www.peppercarrot.com para más información!
Pepper|6|True|Estamos en Patreon, Tipeee, PayPal, Liberapay ... ¡y más sitios!
Pepper|8|False|¡Gracias!
Pepper|2|True|¿Sabías qué?
Credits|1|False|3 de septiembre de 2019 Dibujo & guion: David Revoy. Lectores de la versión beta: Alina the Hedgehog, Craig Maloney, Jihoon Kim, Parnikkapore, Martin Disch, Nicolas Artance, Valvin. Versión en castellano Traducción: Marno van der Maas. Basado en el universo de Hereva Creador: David Revoy. Mantenedor principal: Craig Maloney. Escritores: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correctores: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita/4.2~git branch, Inkscape 0.92.3 on Kubuntu 18.04.2. Licencia: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
