# Transcript of Pepper&Carrot Episode 16 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 16: Vismannen på fjellet

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Safran|9|False|Nei, nei og atter nei, Pepar!
Skrift|2|True|Safrans
Skrift|3|False|hekseri
<hidden>|1|False|★★★
Skrift|4|False|SLAKTAR
<hidden>|7|False|15
<hidden>|6|False|13
Skrift|1|False|Stjerne-gata
Skrift|5|False|Frisør
Lyd|10|True|Glugg
Lyd|11|False|Glugg

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Safran|2|False|Er det noko eg ikkje gjer, så er det å mekla – ikkje eingong for vener. Det kjem det nemleg aldri noko bra ut av.
Safran|1|True|Dette må du ordna opp i sjølv.
Pepar|3|False|Ver så snill, Safran ...
Pepar|4|False|Eg lærer ikkje noko frå gudmødrene! Dei tek berre over huset mitt, klagar over gikta og kjeftar på meg same kva eg gjer ...
Safran|6|True|Høyr her! Du er ei heks – du bør snart læra å oppføra deg som ei!
Safran|8|False|Vis at du er stor jente!
Safran|7|True|Tal med dei sjølv!
Pepar|5|False|Er du heilt sikker på at du ikkje kan hjelpa meg å snakka med dei ...?
Safran|9|False|Greitt, greitt! Eg får vel verta med deg og veksla nokre ord med dei.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Timian|1|False|Ikkje lærer noko?! Kva i alle dagar meiner du?!
Timian|12|False|Og så har du ikkje eingong mot nok til sjølv å seia det til oss!
Timian|14|False|Hadde det ikkje vore for gikta, skulle du fått ein real leksjon! Og ikkje i hekseri, om du skjønar kva eg meiner.
Kajenne|15|True|Ro deg ned, Timian!
Kajenne|16|False|Dei er inne på noko ...
Fugl|2|True|pip
Fugl|3|False|pip
Fugl|4|True|pip
Fugl|5|False|pip
Fugl|6|True|pip
Fugl|7|False|pip
Fugl|8|True|pip
Fugl|9|False|pip
Fugl|10|True|pip
Fugl|11|False|pip
Lyd|13|False|BANK!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Kajenne|1|True|Me er ikkje så unge som me ein gong var, og det er nok på tide med ein liten leksjon.
Kajenne|2|True|Eg føreslår at me derfor tek eit møte med
Timian|5|True|He, he!
Timian|6|True|Vismannen?
Timian|7|False|Er dei ikkje for unge til å forstå læra hans?
Pepar|8|True|For unge til kva?!
Pepar|9|False|Me dreg!
Safran|10|False|«Me»?
Timian|11|True|Ja, så er me her.
Timian|12|True|De kan sjølve presentera dykk for vismannen.
Timian|13|False|De finn han oppe på fjelltoppen.
Timian|14|True|Klare for ein
Timian|15|False|skikkeleg leksjon?
Kajenne|3|True|vismannen på fjellet .
Kajenne|4|False|Lærdommen hans gjer klok!

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Klar!
Safran|2|False|Eg òg!
Timian|3|False|Bra! Det er den rette innstillinga!
Pepar|9|False|TIL ÅTAK!!!
Safran|6|False|Eit ... MONSTER !
Pepar|8|True|DET ER EI FELLE!
Pepar|7|True|EG VISSTE DET!
Pepar|5|False|Å nei!
Timian|4|False|Spring!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|GRAVITAS SPIRALIS!
Safran|2|False|BRASERO INTENSIA!
Lyd|3|False|Plask!
Timian|4|True|Å, ungdommen no til dags ... Skal alltid gå til åtak på alt og alle!
Pepar|7|False|Kva slags leksjonar er det her?!
Bidragsytarar|11|False|Lisens: Creative Commons Attribution 4.0. Programvare: Krita, Blender, G’MIC og Inkscape på Ubuntu.
Bidragsytarar|10|False|Bygd på Hereva-universet skapa av David Revoy, med bidrag frå Craig Maloney og rettingar frå Willem Sonke, Moini, Hali, CGand og Alex Gryson.
Forteljar|8|False|– SLUTT –
Bidragsytarar|9|False|April 2016 – www.peppercarrot.com – Teikna og fortald av David Revoy – Omsett av Karl Ove Hufthammer og Arild Torvund Olsen
Timian|6|False|Når ingenting går som ein vil, er ikkje noko betre enn eit godt, varmt bad! Det er bra både for gikta vår og nervane dykkar!
Timian|5|True|Vismannens leksjon er å gjera som han!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|1|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder, og er sponsa av lesarane. Takk til dei 671 som støtta denne episoden:
Bidragsytarar|2|False|Du òg kan støtta arbeidet med neste episode av Pepar & Gulrot: www.patreon.com/davidrevoy
