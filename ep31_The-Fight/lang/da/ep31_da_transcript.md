# Transcript of Pepper&Carrot Episode 31 [da]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Episode 31: Kampen

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fortæller|1|False|Dysterhøj, Kaosahs hellige høj.
Lyd|2|False|VRooo om !|nowhitespace
Pepper|3|False|Tsk!
Lyd|4|False|VRoo ooom|nowhitespace
Lyd|5|False|Bziuuu !
Pepper|6|False|Tag den!
Lyd|8|False|Shhoo!
Cayenne|9|False|Amatør!
Pepper|10|False|! !|nowhitespace
Lyd|11|False|BAM!|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Dzzzioooo !
Lyd|2|False|KR AK! !!|nowhitespace
Lyd|3|False|KR A A K ! !!|nowhitespace
Pepper|4|False|Ikke så hurtigt!!!
Pepper|5|False|GRAVITATIONAS SKJOLDIUS!
Lyd|6|False|DZZzioo ! !|nowhitespace
Lyd|7|False|Tshkshkk! !|nowhitespace
Lyd|8|False|Tshkshkk! !|nowhitespace
Lyd|9|False|Toc!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|CARROT!
Pepper|2|False|Plan 7-B!
Pepper|4|False|JPEGUS KVALITIS !
Lyd|5|False|Bzi ooo !|nowhitespace
Lyd|3|False|Zoo oo|nowhitespace
Cayenne|6|False|?!!
Cayenne|10|False|Argh!!
Lyd|7|True|G
Lyd|8|True|Z|nowhitespace
Lyd|9|False|Z|nowhitespace
Pepper|11|False|KVALITIS MINIMALIS!
Cayenne|12|False|! !|nowhitespace
Cayenne|13|False|Grr...
Skrift|14|False|2019-11-20-E31P03_V15-final.jpg
Skrift|15|False|Fejl ved indlæsning
Lyd|16|False|KRASH ! !!|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|PYRO BOMBA ATOMICUS!
Lyd|2|False|Frrroooo!
Lyd|3|True|K
Lyd|4|True|A|nowhitespace
Lyd|5|True|B|nowhitespace
Lyd|6|True|O|nowhitespace
Lyd|7|True|O|nowhitespace
Lyd|8|True|M|nowhitespace
Lyd|9|True|!|nowhitespace
Lyd|10|False|!|nowhitespace
Lyd|11|True|BRRR
Lyd|12|False|BRRR
Lyd|13|False|Sch hh hh...|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Pfff...
Cayenne|2|True|Flygte i en mikrodimension i din alder?
Cayenne|3|False|Sikke et patetisk nederlag ...
Pepper|4|True|Forkert!
Pepper|5|False|Et ormehul!
Lyd|6|False|Bzz ! !|nowhitespace
Pepper|7|False|Tak for udgangen, Carrot!
Pepper|8|False|Skakmat mester Cayenne!
Pepper|9|False|GURGES...
Pepper|10|False|...ATER!
Lyd|12|False|Swwwwwwiiiiiiiipppp!!!
Lyd|11|False|V R O O O ! !!|nowhitespace
Timian|13|False|STOP!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Timian|1|True|Jeg sagde: STOP!
Timian|2|False|Så er det nok!
Lyd|3|False|Klack !|nowhitespace
Lyd|4|False|TSHAK !|nowhitespace
Lyd|5|False|TSHK!
Timian|6|True|I to!
Timian|7|True|KOM HER!
Timian|8|False|Med det samme!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Lyd|1|False|Tok!
Lyd|2|False|Tok!
Timian|3|True|Nu har du i tre år sagt: "lige én prøve mere"
Timian|4|False|...Vi skal ikke bruge hele natten på det!
Timian|5|False|Så... ?
Pepper|6|False|...
Cayenne|7|True|OK...
Cayenne|8|True|...Hun kan få sit diplom...
Cayenne|9|False|...men kun med karakteren "bestået".
Skrift|10|True|Kaosah
Skrift|11|True|Diplom
Skrift|12|False|Cayenne
Skrift|14|False|Spidskommen
Skrift|13|False|T i mian
Skrift|15|False|~ til Pepper ~|nowhitespace
Skrift|16|False|Officiel heks
Skrift|17|False|- SLUT -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|I kan også blive tilhængere og få jeres navne skrevet på her!
Pepper|3|True|Pepper & Carrot er frit, gratis, open-source, og sponsoreret af sine læsere.
Pepper|4|False|Denne episode fik støtte af 971 tilhængere!
Pepper|7|True|Gå på www.peppercarrot.com og få mere information!
Pepper|6|True|Vi er på Patreon, Tipeee, PayPal, Liberapay ...og andre!
Pepper|8|False|Tak!
Pepper|2|True|Vidste I det?
Credits|1|False|Den 20. december 2019 Tegning og manuskript: David Revoy Genlæsning i beta-versionen: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Valvin. Dansk Version Oversættelse: Emmiline Alapetite Rettelser: Rikke & Alexandre Alapetite Baseret på Hereva-universet Skabt af: David Revoy. Vedligeholdt af: Craig Maloney. Medforfattere: Craig Maloney, Nartance, Scribblemaniac, Valvin. Rettelser: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Væktøj: Krita 4.2.6appimage, Inkscape 0.92.3 on Kubuntu 18.04-LTS. Licens: Creative Commons Attribution 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
