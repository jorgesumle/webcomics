# Transcript of Pepper&Carrot Episode 29 [jb]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|.i 29 mo'o lisri le spogau be le munje

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|2|True|ro mai
Monster|3|False|fenra le bitmu be le mi munje
Monster|4|False|.i la'a su'o vajni fasnu po le kensa cu rinka tu'a le fenra
Monster|5|True|banro .i ko banro doi le cmalu fenra
Monster|6|False|.i au do vorme fi ma .a'u noi .ai mi co'a cu turni gi'e se selfu
Monster|7|True|.ue sai
Monster|8|False|.i ti mutce cinri
Monster|9|False|.i mi se funca su'o zabna
Narrator|1|False|.i sei zvati be le drata munje

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|1|False|.i ti munje su'o se menli
Monster|2|False|i ko sutra doi le ca ze'i fenra
Monster|3|False|ko banro mu'a'a xa'a'a
la .piper.|4|True|.uo
la .piper.|5|False|.i tai zabna nu salci doi le pendo
la .koriander.|6|False|ju'i .piper. la .safran. e mi pu zi co'a sanji le du'u mi'a djuno ba'e no da le saske be le makfa pe la kalsa
la .safran.|7|False|.i la kalsa ku .i'u nai sai zo'u pei do skicu

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|1|False|.u'i .i milxe nandu fa lo nu ciksi
la .piper.|2|True|.i sa'e nai la kalsa cu se jicmu le si'o jimpe fi le ciste be fo lo se kalsa jo'u lo se rirci
la .piper.|3|False|.y ju'o ru'e mi co'a jimpe
la .koriander.|4|False|.i pei .ua ru'e gau mi do zgana su'o mupli
la .piper.|7|False|.i ko ze'i denpa .i .ai mi penmi su'o srana
la .piper.|8|False|.ua sai
la .piper.|9|False|.srak.
Sound|5|True|srak.
Sound|6|False|. ti za'a dai denlumtci gi'e pu jbini le'i re ti voi tokci
la .piper.|10|True|.i mi pu vimcu le grana gi'e gasnu lo fanta be lo nu da'i ju'o su'o da stapa ra
la .piper.|11|False|.i ti cmalu nu cenba bu'u le barda kalsa ciste .i ku'i ti rinka lo zabna
la .piper.|12|False|.i lo'e simsa cu jicmu la kalsa

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .citcimin.|1|False|.i'inai
la .koriander.|2|False|sei palci ti voi tutci cu pu nenri su'o moklu
la .safran.|3|False|.u'a la .piper. ro roi banli su'o da
la .koriander.|4|False|je'e .piper. ckire fi le ka .y. za'e ciksi
la .koriander.|5|True|.i pe'i cedra lo nu .ei sipna pei
la .koriander.|6|False|.e lo nu su'o prenu cu lumci le xance be vo'a
la .piper.|7|False|ju'i do ko denpa
Sound|8|False|.gunr.
Sound|9|True|.tip.
Sound|10|False|.tip.
Sound|11|False|.vofl.|nowhitespace
la .karot.|12|False|.y.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|1|False|..sil.sil.
Sound|2|False|.dax.
Sound|3|False|.pec.
Sound|4|False|.poj.|nowhitespace
Sound|5|False|.fag.|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|We have a patch for the artwork in case your text is too short or is not easily split in two words. Ask David or Midgard for more information. Look at the Korean version for an example.
<hidden>|0|False|NOTE FOR TRANSLATORS
Monster|7|False|.ai mi gunta
Monster|6|True|mu'a'a'a ro mai
Monster|9|False|.y.
Writing|1|True|te salci jakne
Writing|2|False|sorcu
Sound|3|False|.banr.|nowhitespace
Sound|4|False|.banr.|nowhitespace
Sound|5|False|.bam.
Sound|8|False|.bik.|nowhitespace

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Sound|3|False|.bam.|nowhitespace
Sound|2|False|.bam.|nowhitespace
Sound|1|False|.fer.|nowhitespace
Sound|4|False|.bik.|nowhitespace
Sound|5|False|.poj.|nowhitespace
la .piper.|6|True|ko na xanka doi .karot.
la .piper.|7|False|.i ju'o sa'u su'o le prenu za'o salci.
la .piper.|8|False|.i .o'u dai ko panpi sipna

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monster|5|False|.y.
la .kumin.|1|False|xu fatci .i .ianai la .piper. cu gasnu lo refkusi fasnu gi'e fau bo na sanji ri
la .kaien.|2|False|mi je'a birti le du'u gasnu
la .timian.|3|False|doi lei ninmu mi jinvi le du'u la .piper. cu ro mai bredi
Sound|6|False|.canc.
Sound|4|False|.jelc.|nowhitespace
Narrator|7|False|to le lisri be le nu la .koriander. co'a turni cu mulno toi

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
la .piper.|5|True|.i de'i li nanca bu 2019 masti bu 4 djedi bu 25 co'a gubni .i le pixra .e le lisri cu se finti la'o fy.David Revoy.fy. .i pu cipra tcidu lu'i le lisri poi pu nu'o bredi fa la'o gy.CalimeroTeknik.gy. e la'o gy.Craig Maloney.gy. e la'o gy.Martin Disch.gy. e la'o gy.Midgard.gy. e la'o gy.Nicolas Artance.gy. e la'o gy.Valvin.gy. .i le lojbo xe fanva zo'u fanva fa la gleki .i skicu la .erevas. noi munje zi'e noi finti fa la'o.fy.David Revoy.fy. .i ralju sidju fa la'o gy.Craig Maloney.gy. .i finti lei lisri be la .erevas. fa la'o gy.Craig Maloney.gy. e la'o gy.Nartance.gy. e la'o gy.Scribblemaniac.gy. e la'o gy.Valvin.gy. .i cikre le xe fanva fa la'o gy.Willem Sonke.gy. e la'o gy.Moini.gy. e la'o gy.Hali.gy. e la'o gy.CGand.gy. e la'o gy.Alex Gryson.gy. .i pilno le proga poi du la'o py. Krita 4.1.5~appimage.py. jo'u la'o py. Inkscape 0.92.3.py. jo'u la'o py.Kubuntu 18.04.1.py. i finkrali javni fa la'o jy. Creative Commons Attribution 4.0 .jy .i zoi .urli. www.peppercarrot.com .urli. judri
la .piper.|3|True|la'e la .piper. jo'u la .karot. cu fingubni gi'e se sarji le tcidu
la .piper.|4|False|.i sarji le nu finti le dei lisri kei fa le 960 da poi liste fa di'e
la .piper.|7|True|xu do djuno le du'u
la .piper.|6|True|.i do ji'a ka'e sidju .i je ba'e le cmene be do ba se ciska fi le tai papri
la .piper.|8|False|.i ko tcidu fi la'o .url. www.peppercarrot.com .url. ja'e le nu do djuno fi le tcila
la .piper.|2|True|.i mi'a pilno la'o gy.Patreon.gy. e la'o gy.Tipeee.gy. e la'o gy.PayPal.gy. e la'o gy.Liberapay.gy. e la'o gy. e lo drata
Credits|1|False|ki'e do
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
