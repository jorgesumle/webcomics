# Transcript of Pepper&Carrot Episode 37 [vi]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/en/static14/documentation&page=062_Transcripts
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Tập 37: Nước mắt của Phượng hoàng

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hạt Tiêu|1|True|Phù~
Hạt Tiêu|2|False|Thật tốt là tụi mình đã đến đây!
Hạt Tiêu|3|False|Ồ, hôm nay là ngày họp chợ.
Hạt Tiêu|4|False|Tụi mình kiếm chút gì ăn trước khi leo lên ngọn núi lửa kia nha.
Hạt Tiêu|5|False|Ta biết em sẽ thích điều này mà!

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Writing|1|False|TRUY NÃ
Writing|2|False|Torreya
Writing|3|False|1 00 000Ko
Writing|4|False|Shichimi
Writing|5|False|250 000Ko
Writing|6|False|Hạt Tiêu
Writing|7|False|1 000 000Ko

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hạt Tiêu|1|False|Họ thật sự đã dán cáo thị khắp nơi rồi.
Hạt Tiêu|2|False|Kể cả những nơi xa xôi hẻo lánh nhất.
Hạt Tiêu|3|False|Nào, tụi mình nên ra khỏi đây trước khi bị phát hiện.
Hạt Tiêu|4|False|Trước mắt đang còn những vấn đề lớn hơn nữa...
Hạt Tiêu|5|False|Ánh sáng đó...
Hạt Tiêu|6|False|Chắc chắn là tổ của nó ở đây rồi.
Hạt Tiêu|7|False|Bingo!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|False|Con người kia, cớ sự gì mà nhà ngươi dám đến phiền ta?
Hạt Tiêu|2|False|Xin kính chào, ngài Phượng Hoàng!
Hạt Tiêu|3|False|Thần tên là Hạt Tiêu, phù thủy xứ Chaosah.
Hạt Tiêu|4|False|Gần đây thần đã nhận vào người một lượng lớn Ma lực loài Rồng, và từ đó thứ này đã xuất hiện và càng ngày càng to ra...
Hạt Tiêu|5|False|Nó khiến sức mạnh của thần trở nên vô hiệu và rồi sẽ giết chết thần.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|True|Hmm...
Phoenix|2|False|Ta hiểu rồi...
Hạt Tiêu|3|False|...vậy nên nhà ngươi đến đây vì ngươi cần nước mắt của ta để chữa, phỏng?
Hạt Tiêu|4|False|Dạ, thần chỉ còn lựa chọn này thôi.
Phoenix|5|False|*thở dài*
Phoenix|6|False|...thế, nhà ngươi còn đợi cái gì nữa?
Phoenix|7|False|Làm ta rơi lệ đi chứ.
Phoenix|8|False|Ta cho nhà ngươi đúng một phút!
Hạt Tiêu|9|False|Sao cơ?!
Hạt Tiêu|10|False|Làm ngài khóc ạ?!
Hạt Tiêu|11|False|Nhưng mà thần đâu có biết là...
Hạt Tiêu|12|False|Ý thần là... Chỉ có một phút?!
Hạt Tiêu|13|True|Uh...
Hạt Tiêu|14|True|OK!
Hạt Tiêu|15|False|Xem nào.
Hạt Tiêu|16|True|Hmm... Ngài hãy nghĩ về nạn đói toàn cầu đi.
Hạt Tiêu|17|True|Từ từ từ từ từ từ!
Hạt Tiêu|18|True|Thần có ý này hay hơn:
Hạt Tiêu|19|False|Người thân ngài đã mất.
Hạt Tiêu|20|True|Vẫn không được ạ?
Hạt Tiêu|21|True|Thú cưng bị bỏ rơi thì sao?!
Hạt Tiêu|22|False|Động vật bị bỏ rơi nghe buồn lắm đó...
Phoenix|23|False|...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hạt Tiêu|1|True|Thế, ngài thấy sao ạ?
Hạt Tiêu|2|False|Những điều đó có khiến ngài đổ lệ?
Phoenix|3|False|NHÀ NGƯƠI GIỠN ĐẤY À?!
Phoenix|4|False|CÓ MỖI VẬY THÔI SAO?!
Phoenix|5|True|Lũ này ít ra còn thử làm thơ!
Phoenix|6|True|Viết truyện!
Phoenix|7|True|VẼ TRANH!
Phoenix|8|False|DIỄN KỊCH!
Phoenix|9|True|THẾ MÀ ĐÂY?!
Phoenix|10|False|NHÀ NGƯƠI CÒN CHẲNG CHUẨN BỊ GÌ!
Phoenix|11|True|ĐỦ RỒI ĐẤY; XÙYYY!
Phoenix|12|False|ĐI VỀ ĐI VÀ QUAY LẠI LÚC NÀO NHÀ NGƯƠI SẴN SÀNG!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Hạt Tiêu|1|False|Grr! Nghĩ đi nào, Hạt Tiêu! Nghĩ ra một câu chuyện buồn nào.
Hạt Tiêu|2|True|Mình làm được mà.
Hạt Tiêu|3|False|Mình sẽ làm được.
Hạt Tiêu|4|True|Chịu...
Hạt Tiêu|5|False|Không nghĩ nổi.
Vendor|6|False|Ê! Này! Cút mau!
Hạt Tiêu|7|False|!!
Vendor|8|False|Con mèo kia không có tiền thì đừng có động vào hàng của ta!
Hạt Tiêu|9|True|Ôi không, em lại thế rồi...
Hạt Tiêu|10|True|CÀ RỐT!
Hạt Tiêu|11|False|Nghĩ chuyện giúp ta thay vì suốt ngày nghĩ về cái bụng đói của em chứ!
Hạt Tiêu|12|False|Ô?!
Hạt Tiêu|13|True|À há...
Hạt Tiêu|14|False|Cách này được.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Phoenix|1|True|Ah! Nhà ngươi đã quay lại.
Phoenix|2|False|Nhanh nhể...
Phoenix|3|False|Một con dao thép ư?!
Phoenix|4|False|Không đùa chứ?!
Phoenix|5|False|Nhà ngươi không biết là ta có thể nung chảy kim loại hay sa-...
Sound|6|False|LỘP
Sound|7|False|BỘP
Sound|8|False|ÔI KHÔNG!
Phoenix|9|True|CÁI THỨ NÀY!
Phoenix|10|False|NHÀ NGƯƠI ĂN GIAN!
Phoenix|11|False|Nhanh lên Cà Rốt!
Hạt Tiêu|12|True|Hứng cho ta càng nhiều nước mắt càng tốt!
Hạt Tiêu|13|False|BỔ
Sound|14|False|BỔ
Sound|15|False|BỔ
Sound|16|False|- KẾT THÚC -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|03 tháng Tám, 2022 Tranh và hoạt cảnh : David Revoy. Đọc kiểm: Arlo James Barnes, Benjamin Loubet, Bobby Hiltz, Chloé, Craig Maloney, Estefania de Vasconcellos Guimaraes, GunChleoc, Nicolas Artance, Olivier Jolly, Rhombihexahedron, Valvin. Dịch thuật tiếng Việt: Bùi Bá Hợp Dựa trên vũ trụ giả tưởng Hà Nhất Hạ Tác giả: David Revoy. Trưởng nhóm duy trì: Craig Maloney. Biên kịch: Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Hiệu đính: Willem Sonke, Moini, Hali, CGand, Alex Gryson. Phần mềm: Krita 5.0.5, Inkscape 1.2 chạy trên Fedora 36 KDE Spin. Giấy phép: Creative Commons Attribution 4.0. www.peppercarrot.com
Hạt Tiêu|2|False|Bạn có biết?
Hạt Tiêu|3|False|Hạt Tiêu & Cà Rốt là bộ truyện tranh mạng miễn phí (tự do), mã nguồn mở được tài trợ bởi chính các độc giả.
Hạt Tiêu|4|False|Với tập truyện này, xin được cảm ơn 1058 người tài trợ!
Hạt Tiêu|5|False|Bạn cũng có thể trở thành người tài trợ cho Hạt Tiêu & Cà Rốt và ghi danh ở đây!
Hạt Tiêu|6|False|Chúng tôi có Patreon, Tipeee, PayPal, Liberapay ... và nhiều hơn nữa!
Hạt Tiêu|7|False|Truy cập www.peppercarrot.com để biết thêm chi tiết!
Hạt Tiêu|8|False|Chân thành cảm ơn!
Notes|9|False|You can also translate this page if you want.
Notes|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
Notes|11|False|Beta readers help with the story, proofreaders give feedback about the text.
