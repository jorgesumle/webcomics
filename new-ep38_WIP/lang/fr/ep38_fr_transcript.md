# Transcript of Pepper&Carrot Episode 38 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for more information and documentation.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 38 : La Guérisseuse

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Vinya|1|False|Enfin...
Vinya|2|False|On a réussi à lui régler son compte à celui-là !
Fritz|3|False|Oui, c'était difficile, ça explique pourquoi nombreux y sont restés...
Fritz|4|False|Ça va Brasic ?
Fritz|5|False|Brasic ? ... Brasic !
Fritz|6|False|Zut, j'avais pas vu que t'étais salement amoché !
Vinya|7|False|Guérisseuse !
Vinya|8|False|On a besoin d'aide, VITE !

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Écartez-vous ...
Son|2|False|Dzing
Brasic|3|False|Mes blessures ont disparu ...
Brasic|4|False|...à chaque fois, c'est le même miracle.
Brasic|5|False|Ton talent est incroyable, Guérisseuse !
Brasic|6|False|Ne traînons pas, le butin nous attend !
Fritz|7|False|Bien dit !
Fritz|8|False|Grâce à elle, on est invincibles !
Fritz|9|False|En route pour le prochain monstre !

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Brasic|1|False|Preums !
Vinya|2|False|Non, moi ! Moi !
Pepper|3|False|?
Cayenne|4|False|Tu m'expliques à quoi tu joues, là ?
Cayenne|5|False|Ça fait des mois qu'on te cherche.

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Heuu ça se voit pas ?
Pepper|2|False|J'ai simplement décidé de tout plaquer et de changer de métier.
Pepper|3|False|Dorénavant, je suis la Grande Guérisseuse Peph'Ra comme tu peux le voir.
Cayenne|4|False|...
Pepper|5|False|Mais pourquoi, tu vas me demander ?!
Pepper|6|False|Eh bien c'est simple !
Pepper|7|False|J'EN AI MARRE !
Pepper|8|False|Marre d'être recherchée, marre qu'on se moque de mes sorts, et par-dessus tout : marre de ne pas arriver à trouver du boulot après avoir étudié si longtemps !
Pepper|9|False|Là, au moins, on a besoin de moi et on m'apprécie pour mes qualités !
Brasic|10|False|Guérisseuse !
Brasic|11|False|Ne t'éloigne pas trop !
Brasic|12|False|Vinya s'est pris un sacré coup.
Son|13|False|Dzing

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Voilà.
Pepper|2|False|Quelque chose à ajouter ?!
Cayenne|3|False|Oui.
Cayenne|4|False|Avoir ton nom sur un avis de recherche est normal pour une sorcière de Chaosah.
Cayenne|5|False|C'est même le signe d'une bonne réputation .
Cayenne|6|False|Ignore les moqueries et les qu'en-dira-t-on , et crée-toi ton propre travail si personne ne t'en donne.
Cayenne|7|False|Et surtout : tu n'es pas une guérisseuse.
Cayenne|8|False|Ils le remarqueront tôt ou tard quand tu seras à court de cette incroyable quantité de Larmes de Phénix que tu as sur toi et que tu utilises pour faire croire à tes nouveaux pouvoirs.
Pepper|9|False|Qu... ?!
Pepper|10|False|Comment as-tu ...
Pepper|11|False|...deviné.
Cayenne|12|False|Tsss !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Grrr !!!
Pepper|2|False|Eh bien j'irais en rechercher si j'en ai plus !
Pepper|3|False|Car ÇA , ça MARCHE !
Pepper|4|False|Pas comme tous vos trucs compliqués du Chaos !
Pepper|5|False|G R RR ...!
Brasic|6|False|Attention !
Brasic|7|False|Le monstre va se métamorphoser !
Monstre|8|False|CRi iii iii i ii i ii
Monstre|9|False|ii i ii iiiii i ii i ii

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Monstre|1|False|CRi iii iiii iii iii ii i ii
Monstre|2|False|ii i iii i iiiiiiiiiii i iiiiii
Monstre|3|False|ii i iii i iiiiiiiiiii i iiiiiiiiii iii iiiiiiii iiii iiiiiii iiiii
Son|4|False|CR R R R ! ! !
Son|5|False|CR R R R ! ! !
Son|6|False|CR R R R ! ! !
Son|7|False|Pok ! ! !
Son|8|False|Pok ! ! !
Son|9|False|Pok ! ! !
Son|10|False|Pok ! ! !
Son|11|False|Pok ! ! !
Son|12|False|Pok ! ! !
Monstre|13|False|i ii ii i i i...
Son|14|False|Dzi iio o o !!
Vinya|15|False|Non mais quelle casserole !
Brasic|16|False|Allez, je vais lui faire ravaler son sifflet, moi !
Brasic|17|False|Je fonce !
Brasic|18|False|Garde un oeil sur moi au cas où, Guériss...
Brasic|19|False|Guérisseuse ?

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|OUPS !
Pepper|2|False|Non, non, non !
Brasic|3|False|Rho ho ho !
Brasic|4|False|Regardez ça ! Je crois que notre guérisseuse a été un peu trop impressionnée par le cri de la bête !
Brasic|5|False|HoHo Ho !
Fritz|6|False|HAHA HA HA !
Fritz|7|False|Et en plus, c'est tout bleu !
Vinya|8|False|HAHA HA HA !
Vinya|9|False|Et vous croyez que ça peut guérir aussi ?
Fritz|10|False|Arf, dégueu !
Fritz|11|False|HAHA HA HA !
Brasic|12|False|HOHO HO !
Pepper|13|False|...
Fritz|14|False|HÉÉÉ !
Brasic|15|False|Attends ! C'était juste pour rigoler !
Vinya|16|False|Guérisseuse !
Cayenne|17|False|Bon retour parmi nous.
Titre|18|False|- FIN -

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|Le $$ Avril 2023 Art & scénario : David Revoy. Lecteurs de la version bêta : Arlo James Barnes, Bobby Hiltz, Craig Maloney, Christian Aribaud, Estefania de Vasconcellos Guimaraes, Frédéric Mora, Erik Mondrian, Nicolas Artance, Rhombihexahedron, Valvin, Vinay. Version française originale Basé sur l'univers d'Hereva Créateur : David Revoy. Mainteneur principal : Craig Maloney. Rédacteurs : Craig Maloney, Nicolas Artance, Scribblemaniac, Valvin. Correcteurs : Willem Sonke, Moini, Hali, CGand, Alex Gryson. Logiciels : Krita 5.1.5, Inkscape 1.2 sur Fedora KDE 37 Licence : Creative Commons Attribution 4.0. www.peppercarrot.com
Pepper|2|False|Le saviez-vous ?
Pepper|3|False|Pepper&Carrot est entièrement libre, gratuit, open-source et sponsorisé grâce au mécénat de ses lecteurs.
Pepper|4|False|Cet épisode a reçu le soutien de %%%% mécènes !
Pepper|5|False|Vous pouvez aussi devenir mécène de Pepper&Carrot et avoir votre nom inscrit ici !
Pepper|6|False|Nous sommes sur Patreon, Tipeee, PayPal, Liberapay ... et d'autres !
Pepper|7|False|Allez sur www.peppercarrot.com pour plus d'informations !
Pepper|8|False|Merci !
Notes|9|False|You can also translate this page if you want.
Notes|10|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
Notes|11|False|Beta readers help with the story, proofreaders give feedback about the text.
