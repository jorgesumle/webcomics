# Transcript of Pepper&Carrot Episode 11 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 11 : Les sorcières de Chaosah

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Pepper, tu fais honte à Chaosah.
Thym|2|False|Cayenne a raison; une sorcière de Chaosah digne de ce nom doit inspirer la peur, l'obéissance et le respect.
Cumin|3|False|..et toi, tu offres des cupcakes et du thé même à nos démons*...
Note|4|False|* voir épisode 8: L'anniversaire de Pepper.
Pepper|5|True|Mais...
Pepper|6|False|...marraines...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|4|False|TAIS-TOI !
Cumin|9|False|Tadaaa!
Thym|1|True|Pepper, Tu es certes douée, mais tu es aussi la seule à nous succéder.
Thym|2|False|C'est notre devoir de faire de toi une vraie méchante sorcière de Chaosah.
Pepper|3|False|Mais... je ne veux pas être méchante! C'est ...c'est contraire à ma....
Cayenne|5|True|... ou nous reprendrons tous tes pouvoirs!
Cayenne|6|False|Et tu redeviendras la petite idiote orpheline de Bout-un-Cureuil.
Thym|7|False|Cumin va t'accompagner partout à présent pour te former et nous assurer que tu réussis.
Son|8|False|Pouf!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cumin|1|False|Le nouveau Roi de Acren est trop doux et bon avec ses sujets. Le peuple doit avoir peur de son Roi.
Cumin|2|False|Ta première mission est d'aller intimider ce monarque pour le manipuler.
Cumin|3|False|Une vraie sorcière de Chaosah a de l'influence sur les puissants!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Le Roi ?...
Pepper|2|False|...si jeune ?!
Pepper|3|False|On doit avoir le même âge...
Cumin|4|False|Mais qu'est-ce que tu attends ! Vas-y ! réveille-le, sermone le, fais lui peur !

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|Toi et moi, on est à peu près pareil...
Pepper|3|True|jeunes...
Pepper|4|True|seuls...
Pepper|5|False|...prisonniers de nos destins.
Son|1|False|Dzzz

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thym|6|False|On a beaucoup de travail avec toi Pepper...
Crédits|8|False|09/2015 - Dessin & Scénario : David Revoy
Narrateur|7|False|- FIN -
Pepper|2|False|?!
Thym|4|True|Premier test:
Thym|5|False|ÉCHEC TOTAL !
Son|1|False|Pouf!
Son|3|False|Fleuf !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|1|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat de ses lecteurs. Pour cet épisode, merci aux 502 Mécènes :
Crédits|3|False|https://www.patreon.com/davidrevoy
Crédits|2|True|Vous aussi, devenez mécène de Pepper&Carrot pour le prochain épisode sur
Crédits|4|False|License : Creative Commons Attribution 4.0 Sources : disponibles sur www.peppercarrot.com Logiciels : cet épisode a été dessiné à 100% avec des logiciels libres Krita 2.9.6, G'MIC 1.6.5.2, Inkscape 0.91 sur Linux Mint 17
