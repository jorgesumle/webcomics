# Transcript of Pepper&Carrot Episode 17 [gd]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tiotal|1|False|Eapasod 17: An tòiseachadh ùr

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Carabhaidh|1|True|Ach a Pheabar...
Peabar|3|False|CHAN FHUIRICH! THA MI ’gur FÀGAIL!!
Peabar|4|False|Chan ionnsaich sibh buidseachas ceart dhomh! Falbhaidh mi – gu bana-bhuidsichean an Achd!
Fuaim|5|False|Fru ui s s !|nowhitespace
Carabhaidh|2|False|Nach fuirich thu...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Peabar|1|True|Ceart ma-thà, togamaid oirnn dha dhùthaich laighe nan gealach
Peabar|2|False|Mìnichidh Sidimi dhuinn mar a gheibh sinn ballrachd ann am bana-bhuidsichean an Achd
Peabar|3|False|A Churrain, faigh am mapa ’s a’ chombaist: tha e ro neulach ach am faicinn far a bheil mi ’dol
Peabar|5|True|MURT!
Peabar|6|True|Cadhalaich!
Peabar|7|False|GABH GRÈIM ORM!
Peabar|9|False|Iochd !!!
Fuaim|8|False|TOOOrrrman! !|nowhitespace
Sgrìobhte|4|False|T

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Fuaim|1|False|SAidse ! !|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Uilebheist|6|False|Aah! !|nowhitespace
Uilebheist|5|False|Iach! !|nowhitespace
Peabar|1|False|Sa chiad dol a-mach, na gabh giorag, a Churrain ...
Cìob|2|True|“’S fheàrr dearg-choimhead dubh na mìle mallachd gun fheum! ...
Peabar|7|False|Dhomhsa dheth, b’ fheàrr leam geasag ionnsaighe no dhà ionnsachadh; ach sin agad e ...
Peabar|8|True|a thruaighe ... chan eil sguab no uidheamachd agam tuilleadh,
Peabar|9|False|’s e turas fada a tha romhainn ...
Cìob|3|True|... Dèan cath-sùla riutha.
Cìob|4|False|Faigh làmh an uachdair! ”

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Carabhaidh|4|False|“Ma tha fios agad cò na lusan a ghabhas ithe, bidh fios agad càit an lorg thu deochan deis an aghaidh an acrais! ”
Peabar|6|False|Gun ionnsaicheadh i tè sam bith dhomh!
Peabar|2|True|Tha mi claoidhte cuideachd, a Churrain ...
Peabar|3|False|Agus sinne seachd làithean gun ithe ...
Curran|1|False|Rùdail
Peabar|5|True|Ach deochan da-rìribh ?...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tìom|1|False|“Chan fheum bana-bhuidseach cheart na Dubh-choimeasgachd combaist no mapa. Foghnaidh reultan an speura dhi ”
Peabar|3|True|Tog dhe do ghruaim a Churrain!
Peabar|4|False|Seall, ràinig sinn an t-àite!
Peabar|2|False|... B’ fheàrr leam cùrsa fàidheadaireachd ceart !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|7|False|Ceadachas : Creative Commons Attribution 4.0, Bathar-bog: Krita, G'MIC, Inkscape air Ubuntu
Urram|6|False|Stèidhichte air saoghal Hereva a chaidh a chruthachadh le David Revoy le taic o Craig Maloney. Ceartachadh le Willem Sonke, Moini, Hali, CGand ’s Alex Gryson.
Neach-aithris|4|False|- Deireadh na sgeòil -
Urram|5|False|06/2016 – www.peppercarrot.com – Obair-ealain ⁊ sgeulachd: David Revoy – Eadar-theangachadh: GunChleoc
Peabar|2|False|Seo deireadh na sgeòil.
Sidimi|3|False|...agus tusa dhen bheachd gun do ràinig thu slàn sàbhailte is iad gun dad sam bith ionnsachadh dhut?
Peabar|1|True|...agus sin agad e.

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Urram|1|False|Tha Pepper&Carrot saor is le tùs fosgailte air fad agus ’ga sponsaireadh le taic nan leughadairean. Mòran taing dhan 719 pàtran a thug taic dhan eapasod seo:
Urram|2|False|’S urrainn dhut dol ’nad phàtran Pepper&Carrot cuideachd air www.patreon.com/davidrevoy
