# Transcript of Pepper&Carrot Episode 16 [oc]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Títol|1|False|Episòdi 16 : Lo Savi de la Montanha

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Safran|9|False|Non, non, non e non, Pepper.
Escritura|1|True|Safran
Escritura|2|True|Mascariá
Escritura|3|False|★★★
Escritura|5|False|MASELIÈR
Escritura|6|False|15
Escritura|4|False|13
Escritura|7|False|carrièra Estela
Escritura|8|False|Cofaire
Son|10|True|Glo
Son|11|False|Glo

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Safran|2|False|Ieu fau pas la mediatritz, a mai pas per una amiga. S'acaba totjorn mal aquela mena d'istòria.
Safran|1|True|Es quicòm que te calrà gerir soleta.
Pepper|3|False|Te'n prègui Safran...
Pepper|4|True|Apreni pas res amb mas mairinas. Elas, fan pas qu'esquatar mon ostal, se plànher de lors vièlhs òsses e cridassar contra ieu qué que faga...
Safran|6|True|Escota, siás una masca, fai coma una masca !
Safran|8|False|Coma una grandeta !
Safran|7|True|Parla-lor dirèctament !
Pepper|5|False|Siás segura que me pòs pas venir ajudar a lor ne parlar ?...
Safran|9|False|D'acòrdi d'acòrdi ! Supausi que pòdi venir amb tu per lor ne parlar.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Frigola|1|False|De qué, aprenes pas res ?!
Frigola|12|False|E mai, siás pas pro coratjosa per nos o venir dire soleta ?!
Frigola|14|False|Se mon esquina anava melhor, te balhariái una bona leiçon ! E mai que de mascariá, se veses çò que vòli dire !
Cayenne|15|True|Frigola, arrèsta.
Cayenne|16|False|An rason.
Aucèl|2|True|piu
Aucèl|3|False|piu
Aucèl|4|True|piu
Aucèl|5|False|piu
Aucèl|6|True|piu
Aucèl|7|False|piu
Aucèl|8|True|piu
Aucèl|9|False|piu
Aucèl|10|True|piu
Aucèl|11|False|piu
Son|13|False|PLAC !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|True|Sèm pas pus fòrça jovas e es lo bon moment per una leiçon.
Cayenne|2|True|Prepausi un encontre amb
Frigola|5|True|He he !
Frigola|6|True|Lo Savi ?
Frigola|7|False|Son pas tròp jovas, encara, per compréner aquò ?
Pepper|8|True|Tròp jovas per qué !?
Pepper|9|False|I anem !
Safran|10|False|Qué ?
Frigola|11|True|Aquí, i sèm.
Frigola|12|True|Vos daissi far coneissença amb lo Savi.
Frigola|13|False|Es en naut d'aquesta pujada.
Frigola|14|True|Sèts prèstas a préner
Frigola|15|False|una bona leiçon ?
Cayenne|3|True|lo Savi de la Montanha !
Cayenne|4|False|Son ensenhament es essencial.

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|Prèsta !
Safran|2|False|Prèsta !
Frigola|3|False|Plan, plan ! Bona actitud !
Pepper|9|False|ATAQUEM !!!
Safran|6|False|Un... UN MOSTRE !
Pepper|7|True|ES UNA TRAPA !
Pepper|8|True|O SABIÁI !!!
Pepper|5|False|Ò non !
Frigola|4|False|Zo !

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|GRAVITAS SPIRALIS !
Safran|2|False|BRASERO INTENSIA !
Son|3|False|Flòc !
Frigola|4|True|Aaa, vosautres jovents, totjorn a voler atacar !
Pepper|7|False|Mas qu'es aquò aquestas leiçons ?!
Crèdits|11|False|Licéncia : Creative Commons Attribution 4.0, Logicials : Krita, Blender, G'MIC, Inkscape sus Ubuntu
Crèdits|10|False|Basat sus l'univèrs d'Hereva creat per David Revoy amb las contribucions de Craig Maloney. Correccions de Willem Sonke, Moini, Hali, CGand e Alex Gryson.
Narrateur|8|False|- FIN -
Crèdits|9|False|04/2016 - www.peppercarrot.com - Dessenh & Scenari : David Revoy, correccions: Remi Rampin
Frigola|6|False|Quand res va pas pus, i a pas res coma un bon banh ! Es bon per nòstres reumatismes e per vòstres nèrvis !...
Frigola|5|True|La leiçon del Savi es de l'imitar !

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crèdits|1|False|Pepper&Carrot es completament liure, open source, e esponsorizat mercés al mecenat de sos lectors. Per aqueste episòdi, mercé als 671 mecènas :
Crèdits|2|False|Vos tanben, venètz mecèna de Pepper&Carrot sus www.patreon.com/davidrevoy
