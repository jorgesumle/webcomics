# Transcript of Pepper&Carrot Episode 25 [fr]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titre|1|False|Épisode 25 : Il n'y a pas de raccourci

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Écriture|1|False|CROQUETTES

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Narrateur|13|False|- FIN -
Pepper|1|True|?
Pepper|2|True|?
Pepper|3|True|?
Pepper|4|False|?
Pepper|5|True|?
Pepper|6|True|?
Pepper|7|True|?
Pepper|8|False|?
Pepper|9|True|?
Pepper|10|True|?
Pepper|11|True|?
Pepper|12|False|?

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Crédits|7|False|Vous aussi, devenez mécène de Pepper&Carrot sur www.patreon.com/davidrevoy
Crédits|6|False|Pepper&Carrot est entièrement libre, open-source, et sponsorisé grâce au mécénat de ses lecteurs. Pour cet épisode, merci aux 909 mécènes :
Crédits|1|False|05/2018 - www.peppercarrot.com - Dessin & Scénario : David Revoy - traduction : Nicolas Artance
Crédits|3|False|Basé sur l'univers d'Hereva créé par David Revoy avec les contributions de Craig Maloney. Corrections de Willem Sonke, Moini, Hali, CGand et Alex Gryson.
Crédits|4|False|Logiciels : Krita 4.0.0, Inkscape 0.92.3 sur Kubuntu 17.10
Crédits|5|False|Licence : Creative Commons Attribution 4.0
Crédits|2|False|Relecture pendant la version beta : Nicolas Artance, Imsesaok, Craig Maloney, Midgard, Valvin, xHire et Zveryok.
