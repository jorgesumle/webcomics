# Transcript of Pepper&Carrot Episode 14 [tp]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
nimi|1|False|lipu nanpa luka luka tu tu: kiwen uta akesi

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kumin|6|True|pona a! tenpo pi pali ala li pini!
jan Kumin|7|False|sina ken kama sona e telo pi tenpo pini suli, e wan pona pi telo ni.
jan Kumin|8|True|a... pakala! ko Kiwen Uta Akesi li pini, li lon ni ala.
jan Kumin|9|False|ona li weka la, mi ken ala open e tenpo sona...
sitelen|10|False|ko Kiwen Uta Akesi
sitelen|5|False|33
sitelen|3|False|WAWA NASA
sitelen|2|True|O LUKIN!
sitelen|1|True|JAN PI
sitelen|4|False|LI LON

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Kumin|1|False|jan Kajen o! jan Tume o! mi wile e ko Kiwen Uta Akesi. sina ken ala ken alasa e ijo ona?
jan Kajen|2|False|ma li lete a !! mi wile ala!
jan Tume|3|False|lon... mi sama...
jan Pepa|4|False|ni li ike ala, mi pali e ona!
jan Pepa|6|False|mi kepeken tenpo lili!
jan Pepa|7|True|"lete a", "lete a"!
jan Pepa|8|False|ona li wawa ala!
jan Pepa|9|False|ilo pi kiwen uta en len pi awen sijelo la, mi kama jo e kiwen uta akesi ni!
jan Kumin|5|False|jan Pepa o, awen!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|a a!
jan Pepa|2|False|mi tawaaa ! mi pilin e ni: ken la, pali ni li pali lili ala a! mi sona ala e ni lon tenpo pini.
jan Pepa|3|True|ike!
jan Pepa|4|False|... akesi sewi kon la, pali ni li lili ala lili? lili ala!!

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|soweli Kawa o! ...
jan Pepa|2|True|o kama sin ...
jan Pepa|3|True|... ni li sona tawa ale: akesi pi ko ma li nasin pi utala ala!
jan Pepa|4|False|anu seme...
jan Pepa|5|True|mi o awen... ni li akesi pi wawa suno...
jan Pepa|6|False|... ken la, ona la, ilo ni li ilo pona ala...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
jan Pepa|1|True|pakala.
jan Pepa|2|True|pali mute la, mi ken ala...
jan Pepa|3|False|... mi anpa. mi pini.
jan Pepa|5|False|jan Pepa o pini lon tenpo ala!
jan Pepa|4|True|ala !
waso|7|False|MU-mu-muuu-muuuuuu ! ! !|nowhitespace
sitelen toki|6|False|suno sin li kama...
jan Kumin|8|False|! !|nowhitespace

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
sitelen toki|10|False|- PINI -
mama|11|False|tenpo 01/2016 - musi sitelen & toki li tan jan David Revoy - ante toki li tan jan Ret Samys
sitelen|6|False|uta akesi|nowhitespace
sitelen|5|True|kiwen|nowhitespace
sitelen|4|True|pona pi
sitelen|7|False|mani ala!
kalama|3|False|weka!|nowhitespace
jan Pepa|1|True|sina toki seme? ni li suli, anu seme?
jan Pepa|2|False|nanpa la, mi jo e kiwen uta akesi ale!
jan Kumin|8|True|... taso, jan Pepa o, ko Kiwen Uta Akesi li tan kiwen ala...
jan Kumin|9|False|... li tan kasi. nimi ona li kasi Kiwen Uta Akesi!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
mama|1|False|jan Pepa&soweli Kawa li nasin jo pi jan ale, li nasin Free(libre), li nasin Open-source, li lon tan mani tan jan pona mute. jan 671 li pana e mani la, lipu ni li lon:
mama|2|True|sina ken pana kin e pona tawa lipu kama pi jan Pepa&soweli Kawa:
mama|3|False|https://www.patreon.com/davidrevoy
mama|4|False|nasin lawa: Creative Commons Attribution 4.0 mama pali li lon lipu www.peppercarrot.com ilo: sitelen pi lipu ni li kepeken taso ilo pi nasin jo pi jan ale. ilo pi nasin Libre li ilo Krita 2.9.10, li ilo Inkscape 0.91 lon ilo Linux Mint 17
