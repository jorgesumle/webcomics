# Transcript of Pepper&Carrot Episode 03 [nn]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Tittel|1|False|Episode 3: Dei hemmelege ingrediensane

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Forteljar|1|False|Komona by, marknadsdag
Pepar|2|False|God morgon! Eg vil gjerne ha åtte graskar-stjerner, takk.
Grønsakshandlar|3|False|Ver så god. Det vert 60 Ko*.
Pepar|5|False|Oi sann ...
Merknad|4|False|* Ko = mynteininga i Komona

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Øh, orsak, eg må visst nøya meg med fire ...
Grønsakshandlar|2|False|Grrr...
Safran|3|False|Ver helsa, gode mann. Ver venleg å gjera klart to dusin av alt for meg. Prima varer, som vanleg.
Grønsakshandlar|4|False|Alltid ei glede, frøken Safran!
Safran|5|False|Men her har me jo Pepar!
Safran|6|False|La meg gjetta: Forretningane går strålande ute på landet?
Pepar|7|False|...
Safran|8|False|Eg reknar med at du handlar ingrediensar til trylledrikk-konkurransen i morgon?
Pepar|9|True|... trylledrikk-konkurranse?
Pepar|10|False|... i morgon?

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|4|False|Heldigvis har eg ein heil dag til å førebu meg!
Pepar|5|False|Sjølvsagt skal eg delta!
Skrift|1|False|Komona trylledrikk-konkurranse
Skrift|2|False|LAG DEN BESTE TRYLLEDRIKKEN 50 000 Ko i premie
Skrift|3|False|På azardag kl. 15 Stortorget i Komona

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|2|False|Aha! Eg veit ...
Pepar|3|False|... nett kva eg treng!
Pepar|4|True|Gulrot!
Pepar|5|False|Gjer deg klar! No skal me leita etter ingrediensar.
Pepar|1|False|...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Fyrst treng me nokre doggperler frå mørke uvêrsskyer ...
Pepar|2|False|... og raude bær frå trollskogen ...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|... pluss føniks-eggeskal frå vulkandalen ...
Pepar|2|False|... og til sist ein drope mjølk frå ei ung drakeku.

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepar|1|False|Det var det, Gulrot. Eg trur eg har alt eg treng.
Pepar|2|True|Det ser ...
Pepar|3|False|... perfekt ut.
Pepar|4|True|Mmm...
Pepar|5|True|Beste ... kaffien ... nokosinne!
Pepar|6|False|Akkurat det eg treng når eg skal arbeida heile natta og laga den perfekte trylledrikken til konkurransen i morgon.
Forteljar|7|False|Framhald følgjer ...

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bidragsytarar|4|False|Алексей, 獨孤欣, Albert Westra, Alejandro Flores Prieto, Alex Lusco, Alex Silver, Alex Vandiver, Alexander Sopicki, Andreas Rieger, Andrew, Andrew Grady, Andrey Alekseenko, Anna Orlova, Antan Karmola, Antoine, Aslak Kjølås-Sæverud, Axel Bordelon, Axel Philipsenburg, Ben Evans, Boonsak Watanavisit, Boudewijn Rempt, carlos levischi, Charlotte Lacombe-bar, Chris Sakkas, Christophe Carré, Clara Dexter, Colby Driedger, Conway Scott Smith, Dmitry, Eitan Goldshtrom, Enrico Billich,-epsilon-, Garret Patterson, Gustav Strömbom, Guy Davis, Helmar Suschka, Ilyas Akhmedov, Inga Huang, Irene C., Jean-Baptiste Hebbrecht, JEM, Jessey Wright, John, Jónatan Nilsson, Joseph Bowman, Juanjo Fernández Monreal, Jurgo van den Elzen, Kai-Ting (Danil) Ko, Kasper Hansen, Kathryn Wuerstl, Ken Mingyuan Xia, Liselle, Lorentz Grip, MacCoy, Mandy, Martin Owens, Maurice-Marie Stromer, Mauricio Vega, mefflin ross bullis-bates, Michelle Pereira Garcia, Morten Hellesø Johansen, Nazhif, Nicki Aya, Nicolae Berbece, Nicole Heersema, No Reward, Noah Summers, Noble Hays, Olivier Amrein, Olivier Brun, Oscar Moreno, Pavel Semenov, Peter Moonen, Pierre Vuillemin, Ret Samys, Reuben Tracey, Rustin Simons, Sami T, Sean Adams og Shadefalcon.
Bidragsytarar|2|False|Denne episoden kunne ikkje vore laga utan dei 93 støttespelarane mine:
Bidragsytarar|1|False|Pepar & Gulrot er ein heilt fri teikneserie, med opne kjelder. (Creative Commons Attribution 3.0 Unported. Høgoppløyste kjeldefiler er tilgjengelege for nedlasting.)
Bidragsytarar|3|False|https://www.patreon.com/davidrevoy
Bidragsytarar|6|False|Særskild takk til David Tschumperlé (Gmic) og heile Krita-teamet! Omsetjing: Arild Torvund Olsen og Karl Ove Hufthammer
Bidragsytarar|7|False|Denne episoden er 100 % teikna med fri programvare: Krita og Gmic på Xubuntu (GNU/Linux)
Pepar|5|False|Tusen takk!
