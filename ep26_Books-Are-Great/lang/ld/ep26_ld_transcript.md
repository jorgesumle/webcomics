# Transcript of Pepper&Carrot Episode 26 [ld]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zha wudetha|1|False|Wud 26: Methalethíle Áabe

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|1|False|Bíi methalethíle áabe wi!
Loyud|2|True|Thalethúle hi!
Loyud|3|False|Thi wobalin woháabe hi úthú eril thod wothad wohimá, i di be abesh mathethuth wáa.
Loyud|4|False|Bóodi láad ne: Bíi duholob worathal wohoyi hi áathesha óowababenan úwanú menáthed rathóo wáa.
Loyud|5|False|Izh bere sháad beye lóolonal bedim ib, beyeth raláadeshub...

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Míi le! Denedi thodá úthú yeneth benemeshub úwanú aril thad nasháad radozhenal wa!
Loyud|1|False|...id rumad oyith yenenan...
Zho|2|False|...ébere nahu áath beyóoth wáa!
Loyud|3|False|Methalethúul áabe wi!
Loyud|4|True|“Bóodi oób ne ralithenal thibáhimu rayil.”
Loyud|5|False|Em wáa!
Thod|6|False|ODEZHO
Loyud|7|False|ODEZHO
Thod|8|False|“Bíidi rilrili loláad ne háda nehé wa. Bóo lhith ra ne wi. Bíidi bohí weth wi.”
Zho|9|False|“Bóo láadethéle ne she dathimethu zhubethoth yomenal wa: Bíidi meyod dathimezhub hi itheth dalanal wi.”
Zho|10|False|“Bóo dibóo le wohahí woháanath úwanú dul ne.”
Thod|11|False|Bíi athíle le áabe wa!!
Thod|12|False|Rayahanesh!
Loyud|13|False|This “they” means one person, without specifying their gender. If possible, please try to do the same in your language (suggestion: if your language doesn't have genderless pronouns, maybe you can translate as “the author”). If that doesn't really work well, assume the author is a woman.

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|1|True|Bíi lothel le wa. Menéde menalili ra rul wi.
Loyud|2|False|Bóo lhith ra ne!
Loyud|3|True|Bíi methal áabe hiwan!
Loyud|4|False|Bíi eril meshóo áal thab, id ril wod le worashud woshodeha, i bel le mith Iliyáaninede wa.
Loyud|5|True|Meyahanesh áabe wa!
Loyud|6|False|Bíida shud Ámed wa;

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thod|1|False|“Bíidi beróo eril sháad ne wobohí wowethemu, ril nosháad shodeha Lúul bethu wa.”
Loyud|2|False|Mmm...
Thod|3|False|worahíya wohu wáa?!?
Loyud|4|True|Izh bebáaha?
Loyud|5|True|AÁ...
Loyud|6|False|Redeb le neth!
Loyud|7|True|Lhlhlh...!
Loyud|8|False|Bere lothel beye radowudeth, ébere hith dódozheshub wa!!
Loyud|9|True|“Bóo héedá ne yulisheth wethemu.”
Loyud|10|False|Bíi meham yulish menedebe nuha wa!
Loyud|11|True|adama
Loyud|12|False|adama

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Zho|1|False|Rrrruuum !|nowhitespace
Loyud|2|False|Methad mesháad lezh nude wáa!!
Zho|3|False|Wuuub!
Loyud|4|False|Báa dósháad le "dal" hith bebáanal? Bíi edeláad le úthú di áabe...
Loyud|5|True|Ámed... Báa thad wéedan ne beth?
Loyud|6|False|Mm... thad ra ne wi...
Loyud|7|True|Lhlh...
Loyud|8|True|?!
Loyud|9|False|Lhlhlh!!!
Loyud|10|True|ERIL LOTHEL LE WA!!!
Loyud|11|True|Eril ralorolo be wa!
Loyud|12|False|Rilrili methad medóthal ra áabe abesheth hadihad wa...
Loyud|13|False|Heb!
Zho|14|False|Ílhilh...

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Loyud|1|False|...íizha
Loyud|2|False|YAA A A A A A A ! ! !|nowhitespace
Zho|3|False|S H UM !|nowhitespace
Loyud|4|True|...methalethíle áabe wa!
Loyud|5|False|Ra. Bíi dóon be wa; hadihad, ...
Dedidehá|6|False|- NODI -

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
<hidden>|0|False|Hudi: Creative Commons Attribution 4.0. Bodibod: Krita 4.1, Inkscape 0.92.3 Kubuntu 17.10 benal. Alehala i woban: David Revoy. Dórawulúd dedidewan: Craig Maloney. Wodide wowéedaná: Ambernite, Craig Maloney, CalimeroTeknik, Jookia, Midgard, Nartance, Popolon, Quiralta, Valvin, xHire, Zeograd. Héedan Láadanedim: Yuli i Álo. Bíi nosháad dedide thera Shebedoni bethude wa Elá: David Revoy. Wohun wonayahá: Craig Maloney. Dedidethodá: Craig Maloney, Nartance, Scribblemaniac, Valvin. Dóoná: Willem Sonke, Moini, Hali, CGand, Alex Gryson. 2018, Ahum thabeshin i nibeya www.peppercarrot.com
Dohiná|1|False|Bíi thad naden ne Loyud i Ámed beth www.patreon.com/davidrevoy beha wa
Dohiná|2|False|Bíi dínonehóo Loyud i Ámed wa. Dínon woban betha. Meden wéedaná i meban ben wothem wolotheth. Áala withedim 1098 hi:
Dohiná|3|False|Remove the names of the people who helped to create the English version and create this section for your language. You can credit translators, proofreaders, whatever categories you want. N.B.: In Inkscape, if you press Enter to create a new line, it will not push down text below it until the line has text. So to create a whiteline, type a space on its own line.
