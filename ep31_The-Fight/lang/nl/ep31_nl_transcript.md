# Transcript of Pepper&Carrot Episode 31 [nl]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Titel|1|False|Aflevering 31: Het gevecht

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Verteller|1|False|Duistermiste, de Heilige Heuvel van Chaosah.
Geluid|2|False|VRioee !|nowhitespace
Pepper|3|False|Tss!
Geluid|4|False|VRioeeee|nowhitespace
Geluid|5|False|Bzi oee !
Pepper|6|False|Pak aan!
Geluid|8|False|Schh!
Cayenne|9|False|Amateur!
Pepper|10|False|! !|nowhitespace
Geluid|11|False|BAM !|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|Dzzzioeee !
Geluid|2|False|KRAK! !!|nowhitespace
Geluid|3|False|Krak! !!|nowhitespace
Pepper|4|False|Niet zo snel!!!
Pepper|5|False|SCHILDUS GRAVITATUM!
Geluid|6|False|DZZzioe ! !|nowhitespace
Geluid|7|False|Tchkshkk ! !|nowhitespace
Geluid|8|False|Tchkshkk ! !|nowhitespace
Geluid|9|False|Tok!

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|CARROT!
Pepper|2|False|Plan 7-B!
Pepper|4|False|JPEGUS QUALITIS!
Geluid|5|False|Bzi oee !|nowhitespace
Geluid|3|False|Zo u uu|nowhitespace
Cayenne|6|False|?!!
Cayenne|10|False|Aaah!!
Geluid|7|True|G
Geluid|8|True|Z|nowhitespace
Geluid|9|False|Z|nowhitespace
Pepper|11|False|QUALITIS MINIMALIS!
Cayenne|12|False|! !|nowhitespace
Cayenne|13|False|Grr...
Geschrift|14|False|2019-12-20-E31P03_V15-definitief.jpg
Geschrift|15|False|Er is een probleem opgetreden bij het laden.
Geluid|16|False|CRASH ! !!|nowhitespace

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|PYRO BOMBA ATOMICUS!
Geluid|2|False|Frrzoee!
Geluid|3|True|K
Geluid|4|True|A|nowhitespace
Geluid|5|True|B|nowhitespace
Geluid|6|True|O|nowhitespace
Geluid|7|True|O|nowhitespace
Geluid|8|True|M|nowhitespace
Geluid|9|True|!|nowhitespace
Geluid|10|False|!|nowhitespace
Geluid|11|True|BRRR
Geluid|12|False|BRRR
Geluid|13|False|Psh hh hh...|nowhitespace

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Cayenne|1|False|Pfff...
Cayenne|2|True|Jezelf verschuilen in een microdimensie , op jouw leeftijd?
Cayenne|3|False|Wat een sneue nederlaag...
Pepper|4|True|Mis poes!
Pepper|5|False|Een wormgat!
Geluid|6|False|Bzz! !|nowhitespace
Pepper|7|False|Dankjewel voor de uitweg, Carrot!
Pepper|8|False|Schaakmat, meester Cayenne!
Pepper|9|False|GURGES...
Pepper|10|False|...ATER!
Geluid|12|False|Sjjjjwwwwwoooeeep!!!
Geluid|11|False|V R OEE! !!|nowhitespace
Thym|13|False|STOP!!!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Thym|1|True|Ik zei: STOP!
Thym|2|False|Genoeg zo!
Geluid|3|False|Knip !|nowhitespace
Geluid|4|False|POOEF!|nowhitespace
Geluid|5|False|TCHK !
Thym|6|True|Hier komen!
Thym|7|True|ALLEBEI!
Thym|8|False|En wel nu meteen!

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Geluid|1|False|Tok!
Geluid|2|False|Tok!
Thym|3|True|Het is nu drie jaar geleden dat je ons zei "nog één laatste testje"...
Thym|4|False|...we blijven hier niet de hele nacht aan de gang!
Thym|5|False|Nou...?
Pepper|6|False|...
Cayenne|7|True|GOED...
Cayenne|8|True|...ze mag haar diploma hebben...
Cayenne|9|False|...maar ze krijgt niet meer dan een zesje.
Geschrift|10|True|Diploma
Geschrift|11|True|van
Geschrift|12|False|Chaosah
Geschrift|14|False|Cayenne
Geschrift|13|False|Cumin
Geschrift|15|False|T h ym|nowhitespace
Geschrift|16|False|~ voor Pepper ~
Geschrift|17|False|Erkende Heks
Verteller|18|False|- EINDE -

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|5|True|Jij kan ook patroon van Pepper&Carrot worden en dan komt jouw naam hierbij!
Pepper|3|True|Pepper&Carrot is helemaal vrij,open-bron en gesponsord door de giften van haar lezers.
Pepper|4|False|Voor deze aflevering bedank ik de 971 patronen!
Pepper|7|True|Kijk op www.peppercarrot.com voor alle info!
Pepper|6|True|Je kan doneren met Patreon, Tipeee, PayPal, Liberapay ... en meer!
Pepper|8|False|Dank je!
Pepper|2|True|Wist je dat?
Aftiteling|1|False|20 december 2019 Tekeningen & verhaal: David Revoy. Bèta-feedback: Craig Maloney, Martin Disch, Arlo James Barnes, Nicolas Artance, Valvin. Nederlandstalige versie Vertaling: Julien Bertholon. Proeflezing: Marno van der Maas. Gebaseerd op het Hereva-universum Bedenker: David Revoy. Hoofdonderhouder: Craig Maloney. Schrijvers: Craig Maloney, Nartance, Scribblemaniac, Valvin. Verbeteraars: Willem Sonke, Moini, Hali, CGand, Alex Gryson . Software: Krita 4.2.6appimage, Inkscape 0.92.3 op Kubuntu 18.04-LTS. Licentie: Creative Commons Naamsvermelding 4.0. www.peppercarrot.com
<hidden>|0|False|NOTE FOR TRANSLATORS Replace this section, e.g.: French version Translation: My Name. Proofreading: Other Name.
<hidden>|0|False|You can also translate this page if you want.
<hidden>|0|False|Beta readers help with the story, proofreaders give feedback about the text.
